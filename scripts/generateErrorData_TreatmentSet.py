#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import sys, os
import subprocess
from datetime import datetime
from subprocess import Popen, PIPE, STDOUT


def errorset( scale ):
    ratio = 5
    #maxNumberofOrderLine = 5

    d = round(scale,1)
    multiple = d * 10
    mul = int(multiple)

    a = {      
        "Treatment": 10
    }

    for e, c in a.items():
        if e != "orderLine":
            a[e] = int(c * mul * ratio)
    
    #a["orderLine"] = maxNumberofOrderLine

    return a



def getTreatment( c ):
    s = f"""   
    {{{{relationship("TreatmentTracking-id-idx", "INIT", "1000")}}}}
    ['{{{{repeat({10000})}}}}', 
        {{
            "MRN":{{{{ error("ALTERNATE", 5, integer(1000, 10000), relationship("TreatmentTracking-id-idx", "INCREMENT", "1000")) }}}},
            "Last name":"{{{{ error("BEYOND_CONTEXT", 2, error("TRUNCATE", 2, customLastName()), customFirstName()) }}}}",           
            "First name":"{{{{ error("BEYOND_CONTEXT_NEXT", error("SPELLING", 3, error("ALTERNATE", 3,  customFirstName(), customFirstName()))) }}}}",                        
            "DOB":"{{{{ error("SYNTAX", 10, "date", "dd.MM.yyyy", error("ALTERNATE", 3, date("01-01-1948%%00:00:00","01-10-1991%%00:00:00","dd.MM.yyyy"), put("dob", date("01-01-1948%%00:00:00","01-10-1991%%00:00:00","dd.MM.yyyy")))) }}}}",         
            "Treatment Sessions":"{{{{ error("INTERVALLVIOLATION", 3, 12, 36, put("treats", integer(12, 36))) }}}}",
            "First Treatment Date":"{{{{ put("ftd", error("ALTERNATE", 2, addDays("dd.MM.yyyy", "01.01.2007", toInt(calculate("+", integer(6, 7), calculate("*", integer(0, 51), 7)))), addDays("dd.MM.yyyy", "01.01.2007", toInt(calculate("+", integer(1, 5), calculate("*", integer(0, 51), 7))))))  }}}}",                       
            "Last Treatment Date":"{{{{ error("MISSINGVALUE", 3, addDays("dd.MM.yyyy", get("ftd"), integer(365, 366))) }}}}"       
        }}
    ]
    """
    return s.replace('\n','').replace(' ','').replace('%%',' ')




def writeFile(p, d):
        with open(p, "w") as f:
          f.write(d)    


def executeCmd( cmd ):
    try:
        print("executing shell command: " + cmd)
        """
        p = subprocess.Popen( cmd, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        p.wait()
        #p.stdout.flush()
        ret_code = p.returncode

        if ret_code != 0:
            print("ERROR - , " + str(stderr))
            return None
        #return stdout
        """

        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        
        for line in p.stdout:  
            line = line.rstrip()  
            print(line.decode("utf-8"))

    except subprocess.CalledProcessError as err:
        print("ERROR - executing shell command: " + str(err))
        return None


def genErrorSetSchema( p, scale ):
    for e, c in errorset(scale).items():
        if e == "Treatment":
            writeFile(p + "/01-Treatment.schema", getTreatment(c)) 



def prepare( jar_file ):
    try:
        if not os.path.isfile(jar_file):
            print("WARNING - JAR does not exist! Building it ...")
            executeCmd("mvn -U clean package")
        
        if not os.path.exists("./datasets/"):
            print("WARNING - folder 'datasets' does not exist! Creating it ...")
            os.mkdir("./datasets")
        
        if not os.path.exists("./schema/"):
            print("WARNING - folder 'schema' does not exist! Creating it ...")
            os.mkdir("./schema")
    except Exception as err:
        print("ERROR - creating folder structure and/or building JAR file!")


    

def main(argv):

    if len(argv) > 1 or len(argv) == 0:
        print("ERROR - with arguments")
        print("python generateErrorData.py <scale value>")
        sys.exit()
    
    scale = float(argv[0])
    jar_file = "./target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar"
    folder_name = "errorData_TreatmentSet" + str(scale).replace('.','_')
    schema_folder_path = "./schema/" + folder_name
    dataset_folder_path = "./datasets/" + folder_name

    prepare( jar_file )

    # Generating schema files
    if os.path.exists(schema_folder_path):
        print("WARNING - Schema already exists: " + schema_folder_path)
        print("Skipping generate schema!")
    else:
        os.mkdir(schema_folder_path)
        genErrorSetSchema( schema_folder_path, scale )

    print(errorset(scale))

    #Get time before data generation
    now = datetime.now()

    # Generating data files    
    if os.path.exists(dataset_folder_path):
        print("WARNING - Data set already exists: " + schema_folder_path)
        print("Skipping generate data set!")
    else:
        executeCmd("java -jar -Xmx8g " + jar_file + " -s " + schema_folder_path  + "  -o " + dataset_folder_path)

    #Show TimeStamp for execution time measurement        
    print("")
    current_time = now.strftime("%H:%M:%S.%f")
    print("Start Time =", current_time)
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S.%f")
    print("Finish Time =", current_time)

if __name__ == "__main__":
    main(sys.argv[1:])
