#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import sys, os
import subprocess
from datetime import datetime
from subprocess import Popen, PIPE, STDOUT


def errorset( scale ):
    ratio = 5
    #maxNumberofOrderLine = 5

    d = round(scale,1)
    multiple = d * 10
    mul = int(multiple)

    a = {
        "measurement": 50,
        "tester": 10,
    }

    for e, c in a.items():
        if e != "orderLine":
            a[e] = int(c * mul * ratio)
    
    #a["orderLine"] = maxNumberofOrderLine

    return a



def getMeasurement( c ):
    s = f"""   
    {{{{relationship("measurement-id-idx", "INIT", "0")}}}}
    ['{{{{repeat({c})}}}}',
        {{
            "id":{{{{relationship("measurement-id-idx", "INCREMENT", "0")}}}},
            "city":"{{{{ error("DUPLICATE_ENTITY", 5, error("SPELLING", 15, put("City", city()))) }}}}",
            "county":"{{{{ error("DUPLICATE_ENTITY", error("TRUNCATE", 10, country())) }}}}",
            "location_id":"{{{{ error("DUPLICATE_ENTITY", error("DUPLICATE_ATTR", 7, "location", unique("location", concat(get("City"),"_", random("A", "B", "C", "D"), integer(1, 6))))) }}}}",
            "pH":"{{{{ error("DUPLICATE_ENTITY", put("pH", error("EMPTY_TUPEL", 20, error("NONSENSE", 5, error("INTERVALLVIOLATION", 5, 0.1, 13.9, float(0.1, 13.9, "%.2f")))))) }}}}",
            "pH-control":"{{{{ error("DUPLICATE_ENTITY", error("EMPTY_TUPEL", error("OUTLIER", 2, 7.0, 0.5, "normal"))) }}}}",           
            "date":"{{{{ error("DUPLICATE_ENTITY", error("EMPTY_TUPEL", error("SYNTAX", 10, "date", "dd.MM.yyyy", date("01-01-2000%%00:00:00","01-01-2022%%00:00:00","dd.MM.yyyy")))) }}}}", 
            "result":"{{{{ error("DUPLICATE_ENTITY", error("DERIVED_VALUE_ERROR", 5, "pH_result.txt", "pH")) }}}}"            
        }}
    ]
    """
    return s.replace('\n','').replace(' ','').replace('%%',' ')

    
def getTester( c ):
    s = f"""
    {{{{relationship("tester-id-idx", "INIT", "0")}}}}
    {{{{relationship("measurement-id-idx", "INIT", "0")}}}}
    ['{{{{repeat({c})}}}}',
        {{
            "id":{{{{relationship("tester-id-idx", "INCREMENT", "0")}}}},
            "firstname":"{{{{error("IRRELEVANT", 4, "firstname", error("BEYOND_CONTEXT", 10, customFirstName(), customLastName()))}}}}",    
            "lastname":"{{{{error("IRRELEVANT", "lastname", error("CONTEXT", 6, "cost_per_h", error("BEYOND_CONTEXT_NEXT", customLastName())))}}}}",
            "gender":"{{{{alternate(24, "n/a", alternate(2, "d", alternate(60, "m", "w")))}}}}",
            "wage":"{{{{error("MISSINGVALUE", 5,  alternate(integer(50, 60), alternate(integer(200, 300), alternate(integer(1800, 5900), integer(900, 999))))) }}}}",
            "role":"{{{{error("IRRELEVANT", "word", put("role", error("SETVIOLATION", 10, random("Mittelstuermer", "Torwart", "Fluegel"), random("apprentice", "senior", "expert", "junior")))) }}}}", 
            "cost_per_h":"{{{{error("SYNTAX", 5, "number", "%f", put("cost_per_h", error("DERIVED_VALUE_ERROR", 0, "role_cost_map.txt", "role")))}}}}",
            "hours":"{{{{error("IRRELEVANT", "number", put("hours", integer(4, 8)))}}}}",
            "total_costs":"{{{{error("CALCULATION_ERROR", 20, "*", "cost_per_h", "hours")}}}}",              
            "measurement_id":{{{{relationship("measurement-id-idx", "RANDOM", "0")}}}}
        }}
    ]
    """
    return s.replace('\n','').replace(' ','').replace('%%',' ')





def writeFile(p, d):
        with open(p, "w") as f:
          f.write(d)    


def executeCmd( cmd ):
    try:
        print("executing shell command: " + cmd)
        """
        p = subprocess.Popen( cmd, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        p.wait()
        #p.stdout.flush()
        ret_code = p.returncode

        if ret_code != 0:
            print("ERROR - , " + str(stderr))
            return None
        #return stdout
        """

        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        
        for line in p.stdout:  
            line = line.rstrip()  
            print(line.decode("utf-8"))

    except subprocess.CalledProcessError as err:
        print("ERROR - executing shell command: " + str(err))
        return None


def genErrorSetSchema( p, scale ):
    for e, c in errorset(scale).items():
        if e == "measurement":
            writeFile(p + "/01-Measurement.schema", getMeasurement(c)) 
        if e == "tester":
            writeFile(p + "/02-Tester.schema", getTester(c))



def prepare( jar_file ):
    try:
        if not os.path.isfile(jar_file):
            print("WARNING - JAR does not exist! Building it ...")
            executeCmd("mvn -U clean package")
        
        if not os.path.exists("./datasets/"):
            print("WARNING - folder 'datasets' does not exist! Creating it ...")
            os.mkdir("./datasets")
        
        if not os.path.exists("./schema/"):
            print("WARNING - folder 'schema' does not exist! Creating it ...")
            os.mkdir("./schema")
    except Exception as err:
        print("ERROR - creating folder structure and/or building JAR file!")


    

def main(argv):

    if len(argv) > 1 or len(argv) == 0:
        print("ERROR - with arguments")
        print("python generateErrorData.py <scale value>")
        sys.exit()
    
    scale = float(argv[0])
    jar_file = "./target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar"
    folder_name = "errorData-TestSet" + str(scale).replace('.','_')
    schema_folder_path = "./schema/" + folder_name
    dataset_folder_path = "./datasets/" + folder_name

    prepare( jar_file )

    # Generating schema files
    if os.path.exists(schema_folder_path):
        print("WARNING - Schema already exists: " + schema_folder_path)
        print("Skipping generate schema!")
    else:
        os.mkdir(schema_folder_path)
        genErrorSetSchema( schema_folder_path, scale )

    print(errorset(scale))

    #Get time before data generation
    now = datetime.now()

    # Generating data files    
    if os.path.exists(dataset_folder_path):
        print("WARNING - Data set already exists: " + schema_folder_path)
        print("Skipping generate data set!")
    else:
        executeCmd("java -jar -Xmx8g " + jar_file + " -s " + schema_folder_path  + "  -o " + dataset_folder_path)

    #Show TimeStamp for execution time measurement        
    print("")
    current_time = now.strftime("%H:%M:%S.%f")
    print("Start Time =", current_time)
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S.%f")
    print("Finish Time =", current_time)

if __name__ == "__main__":
    main(sys.argv[1:])
