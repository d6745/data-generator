#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import sys, os
import subprocess
from datetime import datetime
from subprocess import Popen, PIPE, STDOUT


def errorset( scale ):
    ratio = 5
    #maxNumberofOrderLine = 5

    d = round(scale,1)
    multiple = d * 10
    mul = int(multiple)

    a = {      
        "Performance_Test": 10
    }

    for e, c in a.items():
        if e != "orderLine":
            a[e] = int(c * mul * ratio)
    
    #a["orderLine"] = maxNumberofOrderLine

    return a



def getPerformance_Test( c ):
    s = f"""   
    {{{{relationship("Performance_Test-id-idx", "INIT", "1")}}}}
    ['{{{{repeat({100000})}}}}', 
        {{
            "Index":{{{{ relationship("Performance_Test-id-idx", "INCREMENT", "1") }}}},
            "City":"{{{{ error("DUPLICATE_ENTITY", 3, error("IRRELEVANT", 5, "word", city())) }}}}",                             
            "Date":"{{{{ error("DUPLICATE_ENTITY", error("SYNTAX", 5, "date", "dd.MM.yyyy", date("08-05-1945%%00:00:00","03-10-1990%%00:00:00","dd.MM.yyyy"))) }}}}",         
            "Count":"{{{{ error("DUPLICATE_ENTITY", error("NONSENSE", 5, error("INTERVALLVIOLATION", 5, 0, 9999, integer(0, 9999)))) }}}}"   
        }}
    ]
    """
    return s.replace('\n','').replace(' ','').replace('%%',' ')


def writeFile(p, d):
        with open(p, "w") as f:
          f.write(d)    


def executeCmd( cmd ):
    try:
        print("executing shell command: " + cmd)
        """
        p = subprocess.Popen( cmd, shell=True, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        p.wait()
        #p.stdout.flush()
        ret_code = p.returncode

        if ret_code != 0:
            print("ERROR - , " + str(stderr))
            return None
        #return stdout
        """

        p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        
        for line in p.stdout:  
            line = line.rstrip()  
            print(line.decode("utf-8"))

    except subprocess.CalledProcessError as err:
        print("ERROR - executing shell command: " + str(err))
        return None


def genErrorSetSchema( p, scale ):
    for e, c in errorset(scale).items():
        if e == "Performance_Test":
            writeFile(p + "/01-Performance_Test.schema", getPerformance_Test(c)) 



def prepare( jar_file ):
    try:
        if not os.path.isfile(jar_file):
            print("WARNING - JAR does not exist! Building it ...")
            executeCmd("mvn -U clean package")
        
        if not os.path.exists("./datasets/"):
            print("WARNING - folder 'datasets' does not exist! Creating it ...")
            os.mkdir("./datasets")
        
        if not os.path.exists("./schema/"):
            print("WARNING - folder 'schema' does not exist! Creating it ...")
            os.mkdir("./schema")
    except Exception as err:
        print("ERROR - creating folder structure and/or building JAR file!")


    

def main(argv):

    if len(argv) > 1 or len(argv) == 0:
        print("ERROR - with arguments")
        print("python generateErrorData.py <scale value>")
        sys.exit()
    
    scale = float(argv[0])
    jar_file = "./target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar"
    folder_name = "errorDataSet_PerformanceTest" + str(scale).replace('.','_')
    schema_folder_path = "./schema/" + folder_name
    dataset_folder_path = "./datasets/" + folder_name

    prepare( jar_file )

    # Generating schema files
    if os.path.exists(schema_folder_path):
        print("WARNING - Schema already exists: " + schema_folder_path)
        print("Skipping generate schema!")
    else:
        os.mkdir(schema_folder_path)
        genErrorSetSchema( schema_folder_path, scale )

    print(errorset(scale))

    #Get time before data generation
    now = datetime.now()

    # Generating data files    
    if os.path.exists(dataset_folder_path):
        print("WARNING - Data set already exists: " + schema_folder_path)
        print("Skipping generate data set!")
    else:
        executeCmd("java -jar -Xmx8g " + jar_file + " -s " + schema_folder_path  + "  -o " + dataset_folder_path)

    #Show TimeStamp for execution time measurement        
    print("")
    current_time = now.strftime("%H:%M:%S.%f")
    print("Start Time =", current_time)
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S.%f")
    print("Finish Time =", current_time)

if __name__ == "__main__":
    main(sys.argv[1:])
