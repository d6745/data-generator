package de.hda.fbi.datagenerator;

import com.github.vincentrussell.json.datagenerator.JsonDataGeneratorException;
import com.github.vincentrussell.json.datagenerator.impl.JsonDataGeneratorImpl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class EntityTypeGenerator {

    private File inputSchemaFile;
    private File outputFile;
    private OutputStream outputStream = System.out;

    public EntityTypeGenerator(File inputSchemaFile, File outputFile) {
        this.inputSchemaFile = inputSchemaFile;
        this.outputFile = outputFile;
    }

    public void generateEntitiesToSystemOutput(){

        JsonDataGeneratorImpl parser = new JsonDataGeneratorImpl();

        try {
            parser.generateTestDataJson(inputSchemaFile, outputStream);
        } catch (Exception e){
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void generateEntitiesToFile() {

        JsonDataGeneratorImpl parser = new JsonDataGeneratorImpl();


        try {
            System.out.println("\nProcessing schema\n " + inputSchemaFile + "\ninto\n " + outputFile);
            if (outputFile.exists()) {
                outputFile.delete();
                System.out.println(" Note: Target file already exist and is overwritten.");
            }
            final long timeStart = System.currentTimeMillis();
            parser.generateTestDataJson(inputSchemaFile, outputFile);
            final long timeEnd = System.currentTimeMillis();
            System.out.println("Done (" + (timeEnd - timeStart) + "ms)");

        } catch (JsonDataGeneratorException e) {
            System.out.println(e.getStackTrace());
        }
    }

    public File getInputSchemaFile() {
        return inputSchemaFile;
    }

    public void setInputSchemaFile(File inputSchemaFile) {
        this.inputSchemaFile = inputSchemaFile;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }
}
