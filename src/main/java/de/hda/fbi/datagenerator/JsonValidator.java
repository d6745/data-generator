package de.hda.fbi.datagenerator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.apache.commons.io.FilenameUtils;


public class JsonValidator {

    //Gson gson = new Gson();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    SchemaGenerator schemaGenerator = new SchemaGenerator();
    String path = null;


    public JsonValidator() { }

    public File[] validateFolder( String path ) {
        File datadir = new File(path);
        this.path = path;

        if (!datadir.exists()) {
            System.out.println("ERROR - data dir does not exist: " + datadir.getName());
            System.exit(1);
        }
        // Read json files to array
        File[] jsonFiles = datadir.listFiles((dir, name) -> {
            if (name.toLowerCase().endsWith(".json")) {
                return true;
            } else {
                return false;
            }
        });

        boolean valid = true;
        for (File f : jsonFiles) {
            System.out.print("\n  validating JSON file: " + f.getName());
            if (!validateJsonFile(f)){
                valid = false;
            }
        }
        System.out.println("");
        if (valid) {
            return jsonFiles;
        }
        return null;
    }

    // Parsing large JSON files as stream with gson
    // https://sites.google.com/site/gson/streaming
    /*
    public void validateZipFile(File zipFile) {
        try {
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                System.out.println("  processing zip entry: " + zipEntry.getName());
                parseJsonStream(new InputStreamReader(zis, "UTF-8"));
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        } catch (IOException ex) {
            System.out.println("  ERROR - reading zip file");
        }
    }
    */

    public Boolean validateJsonFile(File jsonFile) {
        //System.out.println("  BEGIN validating json file...");
        try {
            String title = FilenameUtils.removeExtension(jsonFile.getName());

            FileInputStream fis = new FileInputStream(jsonFile);
            parseJsonStream(new InputStreamReader(fis, "UTF-8"), title);
        } catch (JsonSyntaxException | IOException ex) {
            System.out.println("  ERROR :(");
            System.out.println("    --> " + ex);
            return false;
        }
        System.out.print(  "  DONE :)");
        return true;
    }

    private void parseJsonStream(InputStreamReader inputStreamReader, String title) throws JsonSyntaxException, IOException {
        JsonReader reader = new JsonReader(inputStreamReader);
        reader.beginArray();

        //Map<String,Object> merged_map = new HashMap<String,Object>();
        JsonObject merged_json_object = new JsonObject();

        while (reader.hasNext()) {

            //Map<String,Object> map = new HashMap<String,Object>();
            //map = (Map<String, Object>) this.gson.fromJson(reader, map.getClass());

            JsonObject json_object = this.gson.fromJson(reader, JsonObject.class);
            json_object.entrySet().forEach(
                (e) -> {
                    if (!merged_json_object.has(e.getKey())){
                        merged_json_object.add(e.getKey(), e.getValue());
                    }
                }
            );

            /*
            map.forEach(
                (k,v) -> {
                    if (!merged_map.containsKey(k)){
                        merged_map.put(k,v);
                    }
                }
            );
            */
        }

        reader.endArray();
        reader.close();

        JsonObject schema = schemaGenerator.getSchemaFromJsonObject(merged_json_object, title);

        Writer writer = Files.newBufferedWriter(Paths.get(path +"/schema_" + title + ".json"));
        gson.toJson(schema, writer);
        writer.close();
    }
}
