package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.impl.*;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import com.github.vincentrussell.json.datagenerator.functions.FunctionRegistry;

import java.io.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.apache.commons.io.FilenameUtils;

public class ErrorToGroundTruth {
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private ErrorCollector concreteErrors = ErrorCollector.getInstance();
    private String path = null;

    public void writeGroundTruth(String path) {
        File datadir = new File(path);
        this.path = path;

		//check if directory exists
        if (!datadir.exists()) {
            System.out.println("ERROR - data dir does not exist: " + datadir.getName());
            System.exit(1);
        }

		//check if erros have been generated
		int n = concreteErrors.getLength();		
		if (n > 0) {

        //read generated json files to array
        File[] jsonFiles = datadir.listFiles((dir, name) -> {
            if (name.toLowerCase().endsWith(".json")) {
                return true;
            } else {
                return false;
            }
        });

		//sort errors and generate GroundTruth
        boolean valid = true;
        for (File f : jsonFiles) {
			concreteErrors.sort();
            generateJsonFile(f);
        }

        System.out.println("");		
		}
		else {
			System.out.println("");	
	        System.out.println("No errors generated. No GroundTruth file(s) generated");
		}
	}
	
	//create Json file for GroundTruth
    public Boolean generateJsonFile(File jsonFile) {
        try {
            String title = FilenameUtils.removeExtension(jsonFile.getName());
            if (!title.contains("schema")){
				System.out.print("\n  generating GroundTruth JSON file for: " + jsonFile.getName());
				FileInputStream fis = new FileInputStream(jsonFile);
				parseJsonStream(new InputStreamReader(fis, "UTF-8"), title);
				System.out.print(  "  DONE :)");
			}
        } catch (JsonSyntaxException | IOException ex) {
            System.out.println("  ERROR :(");
            System.out.println("    --> " + ex);
            return false;
        }
        return true;
    }


	//method to read from generated json data files and correct errors
    private void parseJsonStream(InputStreamReader inputStreamReader, String title) throws JsonSyntaxException, IOException {
        
		//initialize variables
		JsonReader reader = new JsonReader(inputStreamReader);
        reader.beginArray();
		JsonElement ee = null;
		boolean stacked;
		String errorvalue = "";
		String correctvalue = "";
		String input = "";
		int intvalue = 0;	int intcorrectvalue = 0;
		int i = 0; 		int j = 0;
		int counter = 0;
		int n = concreteErrors.getLength();
		
		//check ErrorList for i;
		for(int k=0;k<n;k++){
            if (concreteErrors.getSchema(k).equals(title)) {
				i = k;
				break;
			}
			i = k;
        }
		

		JsonObject merged_json_object = new JsonObject();
		FileWriter writer = new FileWriter(path +"/" + title + "_GroundTruth.json");
		//if error list end is not reached get next error value
		if (i < n) {
			errorvalue = concreteErrors.getReplacement(i);
			correctvalue = concreteErrors.getOrigin(i);
			if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}
				j = i;
				stacked = true;					
				while (j+1 < n && stacked) {
					if ((errorvalue == concreteErrors.getOrigin(j+1)) && concreteErrors.getOrigin(j+1) != "" && concreteErrors.getReplacement(j+1) != "zzz_keinFehler") {
						errorvalue = concreteErrors.getReplacement(j+1);
						if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
						j += 1;
					}
					else {
						stacked = false;
					}					
				}			
			
			//transform errorvalue into JsonObject
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("id", errorvalue);
			for (Map.Entry<String,JsonElement> d : jsonObj.entrySet()) {
				ee = d.getValue();
			}
		} else {errorvalue = null;}		

		//read generated Json files	
        while (reader.hasNext()) {
            JsonObject json_object = this.gson.fromJson(reader, JsonObject.class); 
			counter += 1;
			for (Map.Entry<String,JsonElement> e : json_object.entrySet()) {
                if (!merged_json_object.has(e.getKey())){
                    merged_json_object.add(e.getKey(), e.getValue());
				}

			if (i < n) {
				//check if line to search for error is correct
				if (counter == concreteErrors.getLine(i)) {					
					if (e.getValue().equals(ee)) {

						//if dependend functions, reapply them
						if (concreteErrors.getFunction(i).get(0) == "DERIVED_VALUE_ERROR") {
							correctvalue = correct_derived_value(i, json_object);
						}					
						else if (concreteErrors.getFunction(i).get(0) == "DERIVED_VALUE_CALCULATION_ERROR") {
							correctvalue = correct_derived_value_calc(i, json_object);
						}
						//Since duplicate entity cannot really be corrected it is flagged 
						else if (concreteErrors.getType(i) == "DUPLICATE_ENTITY") {
							correctvalue = "duplicate - removed";							
						}
						
						//replace error with correct value
						json_object.addProperty(e.getKey(), correctvalue); 
						i = j;
						i += 1;

						//get new value to search for correction
						if (i < n) {
							errorvalue = concreteErrors.getReplacement(i);
							correctvalue = concreteErrors.getOrigin(i);

							//all dependend error function values are tracked in the error list, are not corrected but reapplied (e.g. custom calculation error)
							if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
							j = i;
							stacked = true;
							//here multiple errors on one attribute entry are coped with; if it applies the next correct values is drawn
							while (j+1 < n && stacked) {
								if ((errorvalue == concreteErrors.getOrigin(j+1)) && (concreteErrors.getType(j) != concreteErrors.getType(j+1)) && concreteErrors.getReplacement(j+1) != "zzz_keinFehler") {
									errorvalue = concreteErrors.getReplacement(j+1);
									if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
									j += 1;
								}
								else {
									//i = j;
									stacked = false;
								}					
							//transform errorvalue into JsonObject					
							JsonObject jsonObj = new JsonObject();
							jsonObj.addProperty(e.getKey(), errorvalue);
							for (Map.Entry<String,JsonElement> d : jsonObj.entrySet()) {
								ee = d.getValue();
							}
						}
						}
						else {
							errorvalue = null;
						}
					}
					
					//check for non string values
					if (ee != null) {
					try {
					String a = e.getValue().getAsString();
					String b = ee.getAsString();

						intcorrectvalue = Integer.valueOf(correctvalue);
						intvalue = Integer.valueOf(a);					
						if (a.equals(b)) {
						
						json_object.addProperty(e.getKey(), intcorrectvalue); 
						i = j;
						i += 1;
						
						//get new value to search for correction
						if (i < n) {
							errorvalue = concreteErrors.getReplacement(i);
							correctvalue = concreteErrors.getOrigin(i);

							if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
							j = i;
							stacked = true;					
							while (j+1 < n && stacked) {
								if ((errorvalue == concreteErrors.getOrigin(j+1)) && (concreteErrors.getType(j) != concreteErrors.getType(j+1)) && concreteErrors.getReplacement(j+1) != "zzz_keinFehler") {
									errorvalue = concreteErrors.getReplacement(j+1);
									if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
									j += 1;
								}
								else {
									//i = j;
									stacked = false;
								}					
							}	
							//transform errorvalue into JsonObject	
							JsonObject jsonObj = new JsonObject();
							jsonObj.addProperty(e.getKey(), errorvalue);
							for (Map.Entry<String,JsonElement> d : jsonObj.entrySet()) {
								ee = d.getValue();
							}
						}										
						}
					}
					catch (NumberFormatException eee) {
						intcorrectvalue = 0;
						intvalue = 0;	
					}
					
					}
				}
			//if error was not found in line, next error is pulled for next line
			else if (counter > concreteErrors.getLine(i)) {	
			if ((i+1) < n) {
				i += 1;
				errorvalue = concreteErrors.getReplacement(i);
				correctvalue = concreteErrors.getOrigin(i);
					if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
				j = i;
				stacked = true;					
				while (j+1 < n && stacked) {
					if ((errorvalue == concreteErrors.getOrigin(j+1)) && (concreteErrors.getType(j) != concreteErrors.getType(j+1)) && concreteErrors.getReplacement(j+1) != "zzz_keinFehler") {
						errorvalue = concreteErrors.getReplacement(j+1);
						if (errorvalue == "zzz_keinFehler") {errorvalue = correctvalue;}	
						j += 1;
					}
					else {
						//i = j;
						stacked = false;
					}					
				}				
				JsonObject jsonObj = new JsonObject();
				jsonObj.addProperty(e.getKey(), errorvalue);
				for (Map.Entry<String,JsonElement> d : jsonObj.entrySet()) {
					ee = d.getValue();
				}
			}
			}
			}
			}
						
        gson.toJson(json_object, writer);		
		}
	
        writer.close();

	}


    private String correct_derived_value_calc(int i, JsonObject json_object) { 
		if (json_object.get(concreteErrors.getFunction(i).get(2)).getAsString() != null && json_object.get(concreteErrors.getFunction(i).get(3)).getAsString() != null){
			String o1 = json_object.get(concreteErrors.getFunction(i).get(2)).getAsString();
			String o2 = json_object.get(concreteErrors.getFunction(i).get(3)).getAsString();
			CustomCalculation calc = new CustomCalculation();	
			String correctvalue = calc.getCustomCalculation(concreteErrors.getFunction(i).get(1), o1, o2);
			return correctvalue;
		}
		else {
			return null;
		}
	}

    private String correct_derived_value(int i, JsonObject json_object) {  		
		String key = concreteErrors.getFunction(i).get(2); 
		if (json_object.get(key).getAsString() != null){
			String input = json_object.get(key).getAsString();				
			CustomDerivedValueList deri = new CustomDerivedValueList();					
			String correctvalue = deri.getCustomDerivedValueList(concreteErrors.getFunction(i).get(1), input);
			return correctvalue;
		}
		else {
			return "- Error during GroundTruth generation -";
		}

	}
}

//For testing:
//String json = gson.toJson(json_object.get("pH"));
//String json = gson.toJson(json_object);
//System.out.println("json hier: "+ json);	
//System.out.println(" and: " + json_object.get("pH").getAsString());   