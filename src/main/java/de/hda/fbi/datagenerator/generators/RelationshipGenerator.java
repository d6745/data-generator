package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.util.*;
import java.util.stream.Collectors;

@Function(name = "relationship")
public class RelationshipGenerator implements ICustomGenerator {

    private String relationshipName;
    private int relationshipIdentifier;
    private HashSet<Integer> observedIdentifiers;
    private HashMap<String,HashSet<Integer>> relationKeyMap;
	private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
	private Random random = new Random();

    public RelationshipGenerator() {
        this.relationshipIdentifier = 0;
        this.observedIdentifiers = new HashSet<Integer>();
        this.relationKeyMap = new HashMap<String,HashSet<Integer>>();
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
		}		
    }

    @FunctionInvocation
    public String findOrCreateRelationship(String relationshipName, String command, String startingPoint) {

        // Check if the String "command" represents a value in the scope of the RelationshipCommand enum
        RelationshipCommand parsedCommand;

        try {
            parsedCommand = RelationshipCommand.valueOf(command);
        } catch (IllegalArgumentException e) {
            parsedCommand = RelationshipCommand.REPEAT;
        }

        // Check if startingPoint is an actual numbers, if not set to 0
        int parsedStartingPoint;

        try {
            parsedStartingPoint = Integer.parseInt(startingPoint);
        } catch (NumberFormatException e) {
            parsedStartingPoint = 0;
        }

        // Check all relationships if a relationship with the name "relationshipName" was yet defined
        // If so, reuse it (reuses a relationship)
        // If not, it is implicitly created (created a relationship)

        RelationshipGenerator concreteRelationship = RelationshipCollector.findOrCreateRelationship(relationshipName);


        switch (parsedCommand){
            case INIT:
                concreteRelationship.relationshipIdentifier = 0;
                return "";
            case INCREMENT:
                concreteRelationship.incrementRelationshipValue();
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
            case INITTOVALUE:
                concreteRelationship.setRelationshipIdentifier(parsedStartingPoint);
                concreteRelationship.incrementRelationshipValue();
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
            case RANDOM:
                return Integer.toString(concreteRelationship.getRandomRelationshipIdentifier());
            case REPEAT:
            default:
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
        }
    }

    /**
     *
     * @param relationshipName          name of the entity type to get keys from
     * @param relationshipIdentifier    unique name of the relation i.e. "entityType1_entityType2"
     * @return                          next key
     */
    @FunctionInvocation
    public String getNextOneToOneIdentifier(String relationshipName, String relationshipIdentifier) {

        RelationshipGenerator concreteRelationship =
                RelationshipCollector.findOrCreateRelationship(relationshipName);

        return Integer.toString(concreteRelationship.getNextRandomOneToOneKey(relationshipName, relationshipIdentifier));
    }


    @FunctionInvocation
    public String createRandomArray(String relationshipName, String command, String min, String max) {

        RelationshipGenerator concreteRelationship =
                RelationshipCollector.findOrCreateRelationship(relationshipName);

        if (!command.equals("RANDOM_ARRAY")) {
            System.out.println("Ignoring wrong command: " + command);
        }

        int parsed_min, parsed_max;
        try {
            parsed_min = Integer.parseInt(min);
            parsed_max = Integer.parseInt(max);

        } catch (NumberFormatException ex) {
            System.out.println("WARNING - parsing String to int!");
            parsed_min = 0;
            parsed_max = concreteRelationship.observedIdentifiers.size();
        }

        if (Integer.parseInt(max) > concreteRelationship.observedIdentifiers.size()) {
            System.out.println("WARNING - min/max out of range!, " + concreteRelationship.observedIdentifiers.size());
            parsed_max = concreteRelationship.observedIdentifiers.size();
        }



        int arr_size = random.nextInt((parsed_max - parsed_min) + 1) + parsed_min;

        return random.ints(Collections.min(concreteRelationship.observedIdentifiers),
                Collections.max(concreteRelationship.observedIdentifiers)+1)
                .distinct().limit(arr_size).boxed()
                //.map(s -> "\""+s+"\"") // String FKs!
                .collect(Collectors.toList()).toString().replaceAll(" ", "");
    }


    private void incrementRelationshipValue() {
        ++relationshipIdentifier;
		concreteErrors.line = relationshipIdentifier;
        observedIdentifiers.add(relationshipIdentifier);
    }

    public void setRelationshipName(String name) {
        relationshipName = name;
    }

    public String getRelationshipName() {
        return relationshipName;
    }

    public int getRelationShipIdentifier() {
        return relationshipIdentifier;
    }

    // gets a random idenfier based on the observed scope
    public int getRandomRelationshipIdentifier() {
        Random random = new Random();
        int index = random.nextInt(observedIdentifiers.size());

        Iterator<Integer> iterator = observedIdentifiers.iterator();
        for (int i=0; i<index; i++) {
            iterator.next();
        }

        return iterator.next();
    }

    private int getNextRandomOneToOneKey( String relationshipName, String relationshipIdentifier) {
        Random random = new Random();

        HashSet<Integer> localObservesIdentifiers;

        if (relationKeyMap.containsKey(relationshipIdentifier)) {
            localObservesIdentifiers = relationKeyMap.get(relationshipIdentifier);
        } else {
            System.out.println("Adding local one-to-one identifier Set: " + relationshipName + ", " + relationshipIdentifier);
            localObservesIdentifiers = new HashSet<>(observedIdentifiers);
            relationKeyMap.put(relationshipIdentifier, localObservesIdentifiers);
        }

        if (localObservesIdentifiers.size() == 0) {
            System.out.println("ERROR - No more identifier available for the one-to-one relationship!!!");
            System.out.println("Aborting!!!");
            System.exit(1);
        };

        int index = random.nextInt(localObservesIdentifiers.size());

        Iterator<Integer> iterator = localObservesIdentifiers.iterator();
        for (int i=0; i<index; i++) {
            iterator.next();
        }

        int nextId = iterator.next();
        localObservesIdentifiers.remove( nextId );

        return nextId;
    }


    private void setRelationshipIdentifier(int startingPoint) {
        relationshipIdentifier = startingPoint;
        observedIdentifiers.add(relationshipIdentifier);
    }
}
