package de.hda.fbi.datagenerator.generators;

import java.util.*;

abstract class AbstractLineBreakSeparatedFileReader implements ItemReader {

    protected static final String FILE_SEPERATOR = "/"; //System.getProperty("file.separator");
    protected static final String DICTIONARY = "dictionaries";

    private static HashMap<String, ArrayList<String>> dict_cache = new HashMap<String, ArrayList<String>>();

    public String readItem(String filename) {

        try {
            if (!dict_cache.containsKey( filename )){
                // reading file content to buffer
                System.out.println("reading file content to cache, " + filename);
                ArrayList<String> l = new ArrayList<String>();
                for(Scanner sc = new Scanner(getClass().getClassLoader().getResourceAsStream(filename)); sc.hasNext(); )
                {
                     l.add(sc.nextLine());
                }
                dict_cache.put(filename, l);
            }
            String result = null;
            Random rand = new Random();
            int n = 0;
            for(String line : dict_cache.get(filename))
            {
                ++n;
                if(rand.nextInt(n) == 0)
                    result = line;
            }
            return result;

            /*
            String result = null;
            Random rand = new Random();
            int n = 0;
            for(Scanner sc = new Scanner(getClass().getClassLoader().getResourceAsStream(filename)); sc.hasNext(); )
            {
                ++n;
                String line = sc.nextLine();
                if(rand.nextInt(n) == 0)
                    result = line;
            }
            return result;
            */

        } catch (Exception ex) {
            System.out.println("ERROR - reading file!");
            System.out.println(ex);
            return "[!] Dictionary Not Found";
        }
    };

}
