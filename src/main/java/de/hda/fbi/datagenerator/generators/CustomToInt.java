package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.io.File;

@Function(name = "toInt")
public class CustomToInt  implements ICustomGenerator {
boolean db = false;
double parsedv1;
    
	@FunctionInvocation
    public String getInt(String zahl) {
		
		//check if double	
		try{
			parsedv1 = Double.parseDouble(zahl);
			db = true;
		} catch(NumberFormatException e){
			db = false;
		}

		
		if (db) {
			int intzahl = (int) parsedv1;
			return String.valueOf(intzahl);
		}
		else {
			return "NaN";
		} 
	
    }
	
}
