package de.hda.fbi.datagenerator.generators;

import java.util.*;
import java.lang.StringBuilder;
import java.util.Random;
import java.util.SplittableRandom;

public class ErrorSpelling {
private static SplittableRandom RANDOM = new SplittableRandom();	
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();

	//constructor with optional seed
	public ErrorSpelling () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}


    public String getSpellingError(String word) {
		
		if (word.equals("")) {
			return word; 
		}
		 //random decision which error to introduce (equal chances)
		 switch (RANDOM.nextInt(0, 3)){

			case 0:
				return charSwitch(word);
				
			case 1:
				return charDeletion(word);
				
			case 2:
				return charInsert(word);
				
			default:
                return word; 
		 }
		 
	}
		
    private String charSwitch(String wordin) {		
        //convert given word into a char array
        char[] ch = wordin.toCharArray();
  
        //swapping characters
		int i = RANDOM.nextInt(0, ch.length);
		int j = RANDOM.nextInt(0, ch.length);
		if (i == j) {
				if (j != 0) {j -= 1;} else {j += 1;}
		}
        char temp = ch[i];
        ch[i] = ch[j];
        ch[j] = temp;
  
        // Converting the result into a string
        return new String(ch);
    }
 
 
     private String charDeletion(String wordin) {		
		StringBuilder sb = new StringBuilder(wordin);
		
        //delete character
		int k = RANDOM.nextInt(0, sb.length()-1);
		sb.deleteCharAt(k);
		
        return sb.toString();
    }


     private String charInsert(String wordin) {		
		StringBuilder sb = new StringBuilder(wordin);
		Random rand = new Random();
		char c = (char)(rand.nextInt(26) + 'a');
		int k = RANDOM.nextInt(0, sb.length()-1);
		
		//insert character
		sb.insert(k, c);
		
		return sb.toString();

    }

 
}
