package de.hda.fbi.datagenerator.generators;
import java.util.*;
import java.lang.*;


public class UniqueList {

private static UniqueList instance;
private static final SplittableRandom RANDOM = new SplittableRandom();
// unique works with a Map, the key represents the attribute, the string list contains the attribues entries 
private static Map<String, List<String>> dList = new HashMap<String, List<String>>();

	//Private Constructor
	private UniqueList() {

	}

	//Lazy Initialization (Singleton creates the instance in the global access method)
	public static synchronized UniqueList getInstance(){
        if(instance == null){
            instance = new UniqueList();
        }
        return instance;
    }


	public String checkUnique(String attribute, String input) {
		
		//if the attribute has not been used before, add it
		if (!this.dList.containsKey(attribute)) {
			List<String> nlist = new ArrayList<String>(); 	
            this.dList.put(attribute, nlist);
        }

		//if the attributes list already contanis the entry, mark as duplicate
		List<String> l = this.dList.get(attribute);
		if (l.contains(input)) {
			return "!Duplikat!";
		}
		//otherwise add entry to the list and return that entry
		else {
			this.dList.get(attribute).add(input);
			return input;
		}
			
	}
	
	// method to draw a random entry from a attributes unique list
	public String drawUnique(String name, String input) {
		
		if (this.dList.isEmpty()) {
				System.out.println("is empty");
			return input;
		}
				
		if (!this.dList.containsKey(name)) {
			return input;
		}
		
		else {
			List<String> l = this.dList.get(name);	
			int a = l.size();
			
			if (a == 0) {
				return input;
			}
			else {
				return l.get(RANDOM.nextInt(0, a-1));
			}
		}		
	}	

}