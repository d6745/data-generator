package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.util.SplittableRandom;

@Function(name = "alternate")
public class CustomAlternation implements ICustomGenerator {
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
private static SplittableRandom RANDOM = new SplittableRandom();
private int parsedrate;	

	public CustomAlternation () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

	//alternate with specified rate between two given inputs a and b
   @FunctionInvocation
    public String getAlternation(String rate, String a, String b) {

		//reset when a new data object is entered
		if (concreteErrors.alternateLine != concreteErrors.line) {
			concreteErrors.alternate1 = "";
		}
		
		//with probability rate return a instead of b; if a is chosen add 1 to alternate1, otherwise 0 (create binary code)
		concreteErrors.alternateLine = concreteErrors.line;
		rateCheck(rate);
		if (RANDOM.nextInt(1, 101) <= parsedrate) {
			concreteErrors.alternate1 =  concreteErrors.alternate1 + "1";
			concreteErrors.alternate2 = concreteErrors.alternate1;
			return a;
		}
		else {
			concreteErrors.alternate1 =  concreteErrors.alternate1 + "0";
			concreteErrors.alternate2 = concreteErrors.alternate1;					
			return b;
		}
	}

	//alternate in relation to the previous used alternate in function in data object (use without rate)
   @FunctionInvocation
    public String getAlternation(String a, String b) {
		//check if still in the same data object and if first alternate was used
		if (concreteErrors.alternate2.length() == 0 && concreteErrors.alternateLine == concreteErrors.line) {
			concreteErrors.alternate2 = concreteErrors.alternate1;
		}
		
		//alternate between two inputs based on binary code in alternate2
			if (concreteErrors.alternate2.length() > 0) {
				if (Character.toString(concreteErrors.alternate2.charAt(0)).equals("1")) {
					concreteErrors.alternate2 = concreteErrors.alternate2.substring(1, concreteErrors.alternate2.length());
					return a;
				}
				else {
					concreteErrors.alternate2 = concreteErrors.alternate2.substring(1, concreteErrors.alternate2.length());
					return b;
				}
			}
			else {
				return b;
			}
	}


	//rate check
	private void rateCheck(String percentage) {		
        try {
            parsedrate = Integer.parseInt(percentage);
			if  ((parsedrate > 100) || (parsedrate < 0)) {
				parsedrate = 0;
			}
        } catch (NumberFormatException e) {
            System.out.println("The given rate does not comply with the requirements: Interger intervall [0, 100]");
			parsedrate = 0;
        }
	}

}
