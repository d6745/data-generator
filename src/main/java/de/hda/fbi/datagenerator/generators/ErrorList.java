package de.hda.fbi.datagenerator.generators;
import java.util.*;

//class to capture errors
public class ErrorList implements Comparable<ErrorList> {
	private int index;
	private int line;
	private String errorType;
	private String original;
	private String replacedBy;
	private String schema;
	private  List<String> function = new ArrayList<String>();

   //constructor to add an error entry including meta data	
   public ErrorList(String schema, int line, int index, String errorType, String original, String replacedBy, List<String> function) {
		this.schema = schema;
		this.line = line;		
		this.index = index;
		this.errorType = errorType;
        this.original = original;
        this.replacedBy = replacedBy;
        this.function = function;			
   }
   
   //get methods
	public String getListSchema() {
		return this.schema;
	}

    public int getIndex() {
        return this.index;
    }
	
    public int getLine() {
        return this.line;
    }
	
    public String getErrorType() {
        return this.errorType;
    }
	
    public String getOriginal() {
        return this.original;
    }
	
	public String getReplacedBy() {
        return this.replacedBy;
    }

	public List<String> getFunction() {
        return this.function;
    }	
	
	//configure Output
	@Override
    public String toString() {
        return this.schema + "; " + this.line + "; " + this.index + "; " + this.errorType + "; " + this.original + "; " + this.replacedBy + ";";
    }
	
	//method to sort the list	
	public int compareTo(ErrorList el) {
		int result = this.getListSchema().compareTo(el.getListSchema());
		if (result == 0) {
			if (this.getLine() < el.getLine()) {
				result = -1;
			} else if (this.getLine() > el.getLine()) {
				result = 1;
			} else {
				result = 0;
			}				
		}
		if (result == 0) {
			if (this.getIndex() < el.getIndex()) {
				result = -1;
			} else if (this.getIndex() > el.getIndex()) {
				result =  1;
			} else {
				result =  0;
			}	
		}
     return result;
	}	
	
}


