package de.hda.fbi.datagenerator.generators;

import java.util.*;
import java.lang.*;
import java.util.SplittableRandom;
import java.math.*;

public class ErrorOutlier {
	
private static SplittableRandom RANDOM = new SplittableRandom();	
private static final Random random = new Random();	
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();

	//constructor with optional seed
	public ErrorOutlier () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}


    public String getOutlier(String func, int rate, String mean, String var) {			
			
		double parsedmean = 0;
		double parsedvar = 0;

		//check if double
		try{
			parsedmean = Double.parseDouble(mean);
			parsedvar  = Double.parseDouble(var);
		}catch(NumberFormatException e){
			return "";		
		}

		//use customDistribution function to get the regular input		
		CustomDistribution cdist = new CustomDistribution();
		String input = cdist.getCustomDistribution(func, mean, var);
		


		//with the probability of rate an outlier will be returned
		if (RANDOM.nextInt(1, 101) <= rate) {
			String newinput = "";

		if (func.equals("equal")) {
			//use Intervall violation for an outlier of an equal distribution	
				ErrorIntervall intervallError = new ErrorIntervall();
				newinput = intervallError.getIntervallError(mean, var);	
				concreteErrors.addError("OUTLIER", input, newinput);			
				return newinput;	
		}
		else {
			// outlier for other distributions is calcaluted in the same way
			newinput = double_outlier(parsedmean, parsedvar);
			concreteErrors.addError("OUTLIER", input, newinput);		
			return newinput;
		}
		}
		
		else { 
			return input;	
		}
	}
		

	private static String double_outlier(double mean, double var) {
		
		double factor = 1.0;
		//draw a random factor to be multplied with the standard deviation (>= 4)	
		if (random.nextBoolean()) {
			factor = RANDOM.nextDouble(4, 10);
			factor *= (-1);
		} 
		else { 
			factor = RANDOM.nextDouble(4, 10);
		}
			
		double newdin = round(mean + (factor * Math.sqrt(var)), 2);
		return Double.toString(newdin);	
	}
	
	
/*	private static String integer_outlier(double in, double var) {
		
		int factor = RANDOM.nextInt(4, 10);
			
		double newdin = round(in + (factor * Math.sqrt(var)), 0);
		int newin = (int) newdin;
		return Integer.toString(newin);
	}
*/

	// funciton to round
	private static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();
	
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}