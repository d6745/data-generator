package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customAsin")
public class CustomAsinGenerator implements ICustomGenerator {

    @FunctionInvocation
    public String getCustomAsin() {

        int asin = (int)((Math.random() * 999999999 - 100000000) + 1000000000);

        return Integer.toString(asin);
    }
}
