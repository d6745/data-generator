package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.util.Random;
import java.lang.Math;
import java.math.*;

@Function(name = "customDistribution")
public class CustomDistribution extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
private static final Random random = new Random();	

	//constructor with optional seed	
	public CustomDistribution () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
		}
	}


   @FunctionInvocation
    public String getCustomDistribution(String type, String mean, String var) {

			//check if input is double
			boolean floats = true;
			double parsedmean = 0.0;
			double parsedvar = 0.0;
			try{
				parsedmean = Double.parseDouble(mean);
				parsedvar = Double.parseDouble(var);	
			}catch(NumberFormatException e){
                return mean + " / " + var + " = no supported type";
			}
		
		
		//select to draw value from differen distributions based on given type	
		switch (type){
			case "equal":					
					return Double.toString(round(random.nextDouble(parsedmean, parsedvar),2));	
					
			case "normal":
					return Double.toString(round(parsedmean + random.nextGaussian() * parsedvar,2));	
			
			case "poisson":
					double limit = Math.exp(-parsedmean), prod = random.nextDouble();
					int n;
					for (n = 0; prod >= limit; n++)
						prod *= random.nextFloat();
					return Float.toString(n);
					
			default:
                return type + " = no supported distribution";
            
		}
		
	}

	//rounding function
	private static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();
	
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
