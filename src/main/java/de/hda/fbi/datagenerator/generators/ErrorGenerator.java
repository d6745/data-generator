package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.impl.*;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import com.github.vincentrussell.json.datagenerator.functions.FunctionRegistry;

import java.util.*;
import java.util.Random;
import java.util.SplittableRandom;
import java.math.*;

//class for controlling the generation of data errors
@Function(name = "error")
public class ErrorGenerator implements ICustomGenerator {
    
    private int parsedrate;
    private static SplittableRandom RANDOM = new SplittableRandom();
    private static final Random random = new Random();		
	public ErrorCommand parsedCommand;
	private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
    private String input;

	public ErrorGenerator () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

//overloaded getError methods (here with 2 input parameters)	
@FunctionInvocation
    public String getError(String command, String input) {
        //check input (is command in scope of the ErrorCommand enum?) 
		commandCheck(command);
		inputCheck(input);
				
        switch (parsedCommand){

			case BEYOND_CONTEXT_NEXT:
			//if value was already used out of context before the field will be set to ""
				if (concreteErrors.beyondcontext){
					concreteErrors.addError("MISSINGVALUE_BEYOND_CONTEXT", input, "");
					concreteErrors.beyondcontext = false;                    
					return "";
                }
				else {
					return input; 
				}


			case EMPTY_TUPEL:
			//if empty tupel is true for the tupel, return ""
				if (concreteErrors.emptytupel){
						concreteErrors.addError(parsedCommand.toString(), input, "");                    
					return "";
                }
				else {
					return input; 
				}
			

            case DUPLICATE_ENTITY:	
			//introduce duplicate attribute entry for the duplicate entity data error (if duplicate is set to true for the data object)
				boolean db = true;
					if (concreteErrors.duplicate) {
						String newinput = concreteErrors.duplis[concreteErrors.dd][concreteErrors.ddd];
						concreteErrors.ddd += 1;  
						try{
							Double parsedinput = Double.parseDouble(input);
							db = true;
						} catch(NumberFormatException e){
							db = false;
						}
						
						//in 20% of the cases add a spelling error to the duplicate 
						if (!db) {
							if (RANDOM.nextInt(1, 101) <= 20){
								ErrorSpelling spellError = new ErrorSpelling();
								newinput = spellError.getSpellingError(newinput);
							}
						}
						
						concreteErrors.addError(parsedCommand.toString(), input, newinput);						
						return newinput;
					}
					//if duplicate is set to false for the data object, store the value  
					else {	
						if (concreteErrors.storeduplicate) {
							concreteErrors.duplis[concreteErrors.d][concreteErrors.ddd] = input;
							concreteErrors.ddd += 1;
						}
						return input; 
					}

					
			default:
                return input; 
        }		
	}

//overloaded getError methods (here with 3 input parameters)	
@FunctionInvocation
    public String getError(String command, String rate, String input) {
        // Check inputs (command in scope of the ErrorCommand enum and rate is integer between (0, 100)
		commandCheck(command);
		inputCheck(input);

        switch (parsedCommand){

			case MISSINGVALUE:
            //introduce empty values with given rate			
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate){
						concreteErrors.addError(parsedCommand.toString(), input, "");                    
					return "";
                }
				else {
					return input; 
				}
            

			case EMPTY_TUPEL:
           //introduce empty tupels with given rate (subsequent attributes will check if concreteErrors.emptytupel is true)
				rateCheck(rate);
				concreteErrors.emptytupel = false;	
				if (RANDOM.nextInt(1, 101) <= parsedrate){
						concreteErrors.emptytupel = true;		
						concreteErrors.addError(parsedCommand.toString(), input, "");                    
					return "";
                }
				else {
					return input; 
				}


			case IRRELEVANT:
			//introduce irrelevant tupels values with given rate (if concreteErrors.irrelevant is true)
				if (concreteErrors.irrelevant){
						ErrorIrrelevantEntry irr = new ErrorIrrelevantEntry();
						String newinput = irr.getIrrelevantEntry(rate);
						concreteErrors.addError(parsedCommand.toString(), input, newinput);                    
					return newinput;
                }
				else {
					return input; 
				}


            case SPELLING:		
			//introduce spelling error to word with given rate
				rateCheck(rate);
				if (this.isWord(input)) {
					if (RANDOM.nextInt(1, 101) <= parsedrate) {
						ErrorSpelling spellError = new ErrorSpelling();
						String newinput = spellError.getSpellingError(input);
						if (input != newinput){
							concreteErrors.addError(parsedCommand.toString(), input, newinput);
							return newinput;
						}
						else {
							return input; 
						}						
					}
					else {
						return input; 
					}
				}
				else {
					return input; 
				}
						

			case NONSENSE:
			//introduce random nonsense string with given rate				
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate){				
					String newinput = getAlphaNumericString();
					concreteErrors.addError(parsedCommand.toString(), input, newinput);
					return newinput;					
				}
				else {
					return input; 
				}					


			case TRUNCATE:
			//truncate 1/3 of a string from the right
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate){				
					String newinput = "Fehler_at_TRUNCATE";
					int length = input.length();
					int newlength = (int) Math.ceil(length*0.66);				
					if (input != null && input.length() > newlength)
						newinput = input.substring(0, newlength);
						concreteErrors.addError(parsedCommand.toString(), input, newinput);
					return newinput;					
				}
				else {
					return input; 
				}		


            case BEYOND_CONTEXT:
			//introduce values out of context, if triggers, value will be placed in next field instead of here
				rateCheck(rate);
					if (RANDOM.nextInt(1, 101) <= parsedrate) {
						concreteErrors.beyondcontext = true;					
						concreteErrors.addError(parsedCommand.toString(), input, "");	
						return "";
					}
					else {
						return input; 
					}


            case BEYOND_CONTEXT_NEXT:
			//introduce values out of context (requires context value in "attribute" and potential a additional in "input")
					if (concreteErrors.beyondcontext) {
						String newinput = rate + " " + input;
						concreteErrors.addError("MISSINGVALUE_BEYOND_CONTEXT", input, newinput);	
						concreteErrors.beyondcontext = false;						
						return newinput;
					}
					else {
						return input; 
					}


            case DUPLICATE_ENTITY:
			//introduce duplicate entities
				rateCheck(rate);
				String newinput = "";
				concreteErrors.duplicate = false;
					//duplicates will only be introduced starting from line 3
					if (concreteErrors.line > 2 && RANDOM.nextInt(1, 101) <= parsedrate) {
						concreteErrors.duplicate = true;
						concreteErrors.ddd = 1;
						if (concreteErrors.line < 20) {
							concreteErrors.dd = RANDOM.nextInt(0, concreteErrors.d);
							newinput = concreteErrors.duplis[concreteErrors.dd][0];
						}
						else {
							concreteErrors.dd = RANDOM.nextInt(0, 19);							
							newinput = concreteErrors.duplis[concreteErrors.dd][0];							
						}
						concreteErrors.addError(parsedCommand.toString(), input, newinput);						
						return newinput;
					}
					//if duplicate is set to false for the data object, store the value  
					else {				
						if (concreteErrors.d == 19) {concreteErrors.d = 0;} else {concreteErrors.d += 1;}
						int i = (int)(1.0/(concreteErrors.line/20.0)*100);						
						if (RANDOM.nextInt(1, 101) <= i) {
							concreteErrors.storeduplicate = true;
							concreteErrors.ddd = 1;
							concreteErrors.duplis[concreteErrors.d][0] = input;
						}
						else {
							concreteErrors.storeduplicate = false;
						}
						return input; 
					}
				
			
			default:
                return input; 
        }
				

	}


//overloaded getError methods (here with 4 input parameters)	
@FunctionInvocation
    public String getError(String command, String rate, String attribute, String input) {
        //check input (command in scope of the ErrorCommand enum)
		commandCheck(command);
		inputCheck(input);
		
        switch (parsedCommand) {        

            case ALTERNATE:
			//introduce alternate values with given rate (requires other function or value in "attribute" in addition to "input")
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate) {
					String newinput = attribute;
					newinput = attribute;
					concreteErrors.addError(parsedCommand.toString(), input, newinput);	
					return newinput;
				}
				else {
					return input; 
				}


            case BEYOND_CONTEXT:
			//introduce values out of context (requires context value in "attribute" and potential addtional in "input")
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate) {
					concreteErrors.beyondcontext = true;
					String newinput = attribute + " " + input;
					input = attribute;
					concreteErrors.addError(parsedCommand.toString(), input, newinput);	
					return newinput;
				}
				else {
					input = attribute;
					return input; 
				}


            case CONTEXT:
			//introduce out of context values (requires put() function in schema)
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate) {
					Get getkey = new Get();	
					try {
						String newinput = getkey.get(attribute);
						concreteErrors.addError(parsedCommand.toString(), input, newinput);						
						return newinput;
					}						
					catch (IllegalArgumentException e){
						concreteErrors.addError(parsedCommand.toString(), input, attribute);	
						return attribute; 
					}		
				}
				else {
					return input; 
				}

		case DERIVED_VALUE_ERROR:
			//introduce derived value errros (requires put() function in schema for the key)
			rateCheck(rate);
			List<String> stringList = new ArrayList<String>();
			String a = "DERIVED_VALUE_ERROR";
			stringList.add(a); stringList.add(attribute); stringList.add(input);	
			CustomDerivedValueList deri = new CustomDerivedValueList();
			Get getkey = new Get();	
			String key = getkey.get(input);
			input = deri.getCustomDerivedValueList(attribute, key);
			if (RANDOM.nextInt(1, 101) <= parsedrate) {
				String newinput = deri.getCustomDerivedValueListwithErrors(attribute, key, input);
				concreteErrors.addError(command, input, newinput, stringList);
				return newinput;				
			}
			else {
				concreteErrors.addError(command, input, "zzz_keinFehler", stringList);
				return input;
			}
			

            case DUPLICATE_ATTR:
			//introduce duplicates (only works with unique() function that ensures unique values in an attribute)
				rateCheck(rate);					
				if (input != "" && input != "- INVALID -") {
					if (RANDOM.nextInt(1, 101) <= parsedrate) {
						try {
							UniqueList concreteUniq = UniqueList.getInstance();
							String newinput = concreteUniq.drawUnique(attribute, input);
							concreteErrors.addError(parsedCommand.toString(), input, newinput);						
							return newinput;
						}
						catch (IllegalArgumentException e){
							return input; 
						}
					}
					else {
						return input; 
					}
				}	
				else {
					return input; 
				}		


			case IRRELEVANT:
			//introduce irrelevant tupels values with given rate (subsequent attributes will check if concreteErrors.irrelevant is true)
				rateCheck(rate);
				concreteErrors.irrelevant = false;	
				if (RANDOM.nextInt(1, 101) <= parsedrate){				
						concreteErrors.irrelevant = true;
						ErrorIrrelevantEntry irr = new ErrorIrrelevantEntry();
						String newinput = irr.getIrrelevantEntry(attribute);						
						concreteErrors.addError(parsedCommand.toString(), input, newinput);                    
					return newinput;
                }
				else {
					return input; 
				}

			
			case SETVIOLATION:
			//add values from specified setfile with given rate
				rateCheck(rate);
				if (RANDOM.nextInt(1, 101) <= parsedrate) {
					CustomSet cset = new CustomSet();
					String newinput = cset.getSet(attribute);
					concreteErrors.addError(parsedCommand.toString(), input, newinput);						
					return newinput;
				}
				else {
					return input; 
				}
			
			default:
                return input; 
        }
	}



//overloaded getError methods (here with 5 input parameters)	
@FunctionInvocation
    public String getError(String command, String rate, String min, String max, String input) {
   		commandCheck(command);
		rateCheck(rate);
		inputCheck(input);
		
		switch (parsedCommand){

		case INTERVALLVIOLATION:
        //introduce an intervall violations to double or int intervalls with given rate
			if (RANDOM.nextInt(1, 101) <= parsedrate) {
				ErrorIntervall intervallError = new ErrorIntervall();
				String newinput = intervallError.getIntervallError(min, max);
				concreteErrors.addError(parsedCommand.toString(), input, newinput);
				return newinput; 
			}
			else {
				return input; 
			}

		case CALCULATION_ERROR:
        //introduce a calculation error with given rate	(with or without put() in schema)	
			rateCheck(rate);
			String o1 = "";
			String o2 = "";			
			List<String> stringList = new ArrayList<String>();
			String b = "CALCULATION_ERROR";			
			stringList.add(b); stringList.add(min); stringList.add(max); stringList.add(input);	
			Get getkey = new Get();	
			CustomCalculation calc = new CustomCalculation();	
			try {
				o1 = getkey.get(max);
				o2 = getkey.get(input);		
			}
			catch (IllegalArgumentException e) {
				o1 = max;
				o2 = input;				
			}			
				
			input = calc.getCustomCalculation(min, o1, o2);	
			if (RANDOM.nextInt(1, 101) <= parsedrate) {
				String newinput = calc.getCustomCalculationwithErrors(min, o1, o2);
				concreteErrors.addError(command, input, newinput, stringList);
				return newinput;	
			}
			else {
				concreteErrors.addError(command, input, "zzz_keinFehler", stringList);
				return input;
			}


		case OUTLIER:		
		//introduce outlier within a distribtion with a given rate
			ErrorOutlier outlier = new ErrorOutlier();		
			try {
				String newinput = outlier.getOutlier(input, parsedrate, min, max);
				return newinput;
			}
			catch (IllegalArgumentException e) {
				return "Error while ggenerating data for distribution & outliers";
			}


		case SYNTAX:
		//add syntax violations to date, string or numeric values with given rate	
			String newinput = input;
			if (RANDOM.nextInt(1, 101) <= parsedrate) {
				ErrorSyntax syntaxError = new ErrorSyntax();
				if (min.equals("date")) {
					newinput = syntaxError.getOtherDateFormat(max, input);
				}
				if (min.equals("text")) {
					newinput = syntaxError.getOtherTextFormat(max, input);
				}
				if (min.equals("numeric")) {
					if (max.contains("%")) {
						newinput = syntaxError.getOtherNumberFormat(max, input);
					}
				}				
				
				if (input != newinput){
					concreteErrors.addError(parsedCommand.toString(), input, newinput);
					return newinput;
				}
				else {
					return input; 
				}
			}
			else {
				return input; 
			}			
	
		default:
			return input; 
		}
	}


//method to check if command is acceptable
	private void commandCheck(String commando) {	
		try {
			String c = commando;
            parsedCommand = ErrorCommand.valueOf(commando);
        } catch (IllegalArgumentException e) {
			parsedCommand = ErrorCommand.NONE;
        }
	}
	

//check if rate is valid	
	private void rateCheck(String percentage) {		
        try {
            parsedrate = Integer.parseInt(percentage);
			if  ((parsedrate > 100) || (parsedrate < 0)) {
				parsedrate = 0;
			}
        } catch (NumberFormatException e) {
            System.out.println("The given rate does not comply with the requirements: Interger intervall [0, 100]");
			parsedrate = 0;
        }
	}
	
	
//method for the handling of "Umlaute"		
	private void inputCheck(String inp) {		
		if (inp.contains("ö")) {
			inp.replace("ö", "oe");
		}
		if (inp.contains("ü")) {
			inp.replace("ü", "ue");
		}
		if (inp.contains("a")) {
			inp.replace("a", "ae");
		}		
		input = inp;
	}


//method to check if a string content is a word
	private boolean isWord(String word) {
		char[] chars = word.toCharArray();

       //check if string is null or empty
		if (word == null || word.isEmpty()) {
			return false;
		}
			
		for (char c : chars) {
			if(!Character.isLetter(c)) {
				return false;
			}
		}
		return true;
	}
		
 
 //method to generate a random string
    private static String getAlphaNumericString() {
        int n = RANDOM.nextInt(1, 16);
		 
		//chose random chars from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
  
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                = (int)(AlphaNumericString.length()* Math.random());
  
            //add Character one by one
            sb.append(AlphaNumericString.charAt(index));
        }
          return sb.toString();
    }

}

