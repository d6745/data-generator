package de.hda.fbi.datagenerator.generators;
import java.util.*;
import java.io.*;

public class ErrorCollector {

    private static ErrorCollector instance;
	private ArrayList<ErrorList> rowList = new ArrayList<ErrorList>();
	//counter variable 
	private int n = 0;
	
	//global variables for errors
	public boolean beyondcontext = false;
	public boolean emptytupel = false;	
	public boolean irrelevant = false;	
	public String alternate1 = "";
	public String alternate2 = "";
	public int alternateLine = 0;
	public boolean duplicate = false;
	public boolean storeduplicate = false;
	public int d = -1;	public int dd = 0;	public int ddd = 0;  		
	public String[][] duplis = new String[20][20]; 
	public String currentschema;
	public long s = 0;		
	public int line = 0;

	//private constructor	
    private ErrorCollector(){

	}	
	
	//lazy Initialization (Singleton creates the instance in the global access method)
    public static synchronized ErrorCollector getInstance(){
        if(instance == null){
            instance = new ErrorCollector();
        }
        return instance;
    }
	
	//method to add an error to the list (with a n/a for the function entry)
	public void addError(String errorType, String original, String replacedBy) {
		n++;
		List<String> aList = new ArrayList<>();
		String a = "na";
		aList.add(a);
		this.rowList.add(new ErrorList(currentschema, line, n, errorType, original, replacedBy, aList));
	}

	//method to add an error to the list with applied function
	public void addError(String errorType, String original, String replacedBy, List<String> function) {
		n++;
		this.rowList.add(new ErrorList(currentschema, line, n, errorType, original, replacedBy, function));
	}
	
	//methods to print error list	
	public void printErrors() {
		for (ErrorList z: rowList) {
			System.out.println(z);
		}
	}

	//get methods
	public String getReplacement(int i) {
		ErrorList a = rowList.get(i);
		return a.getReplacedBy();
	}

	public String getOrigin(int i) {
		ErrorList a = rowList.get(i);
		return a.getOriginal();
	}

	public String getSchema(int i) {
		ErrorList a = rowList.get(i);
		return a.getListSchema();
	}

	public int getLine(int i) {
		ErrorList a = rowList.get(i);
		return a.getLine();
	}

	public int getIndex(int i) {
		ErrorList a = rowList.get(i);
		return a.getIndex();
	}
	
	public String getType(int i) {
		ErrorList a = rowList.get(i);
		return a.getErrorType();
	}	

	public List<String> getFunction(int i) {
		ErrorList a = rowList.get(i);
        return a.getFunction();
    }	
	
	public int getLength() {
		return rowList.size();
	}

	//set seed
	public void setSeed(long wert) {
		s = wert;
	}
	
			
	//write errorlist to file
	public void writeErrorFile(String path) {	
		try {
			File myObj = new File(path + "Included_Errors.txt");
			if (myObj.createNewFile()) {
			System.out.println("File created: " + path + myObj.getName());
		} else {
			System.out.println("Included_Errors.txt file already exists.");
		}
		} catch (IOException e) {
			System.out.println("An error occurred while creating the error file.");
			e.printStackTrace();
		}

		try {
			FileWriter writer = new FileWriter(path + "Included_Errors.txt");
		
			for(ErrorList l: rowList) {
				if (l.getReplacedBy() != "zzz_keinFehler") {
					writer.write(l + System.lineSeparator());
				}
			}
			
			writer.close();
			System.out.println("Successfully wrote to the file:" + path + "Included_Errors.txt");
		} catch (IOException e) {
			System.out.println("An error occurred while writing to the error file.");
			e.printStackTrace();
		}
	}

	//methods to sort list
	public void sort() {	
        Collections.sort(rowList);
	}
	
}


