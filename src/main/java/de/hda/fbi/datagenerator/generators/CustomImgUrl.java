package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customImgUrl")
public class CustomImgUrl extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "unibench-imageURLs.txt";

    @FunctionInvocation
    public String getCustomImgUrl() {
        return super.readItem(DICTIONARY_FILE);
    }
}
