package de.hda.fbi.datagenerator.generators;

import java.util.*;
import java.lang.StringBuilder;
import java.util.Random;
import java.util.SplittableRandom;
import java.text.*;
//import java.text.DateFormat;
//import java.text.ParseException; 
//import java.text.SimpleDateFormat;

public class ErrorSyntax {
private static SplittableRandom RANDOM = new SplittableRandom();	
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();

	//constructor with optional seed
	public ErrorSyntax () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

	//Syntax error for text format
    public String getOtherTextFormat(String format, String text) {
		String newinput = text;
		//in case of spaces replace by slash
		if (text.contains(" ")) {
			return text.replace(" ", "/");
		}
		//in case of a comma replace by three dots
		else if (text.contains(",")) {
			return text.replace(",", "...");
		}
		//in case of empty string add some "A, B, C"		
		else if (text.equals("")) {
			return "A, B, C";
		}		
		//else tranform to lower case and add brackets
		else {
			char c[] = text.toCharArray();
			c[0] = Character.toLowerCase(c[0]);
			text = new String(c);
			
			return ("(" + text + ")");
		}
	}

	//Syntax error for number format
    public String getOtherNumberFormat(String format, String number) {

		//if integer, change format	
		String newinput = number;
		if (format.contains("d")) {
			try{
				int parsedv1 = Integer.parseInt(number);
			}
			catch (NumberFormatException e){
				return "Not numeric";
			}
			return newintformat(format, number);
		}

		//if float, change format			
		if (format.contains("f")) {
			try{
				double parsedv2 = Double.parseDouble(number);
			}
			catch (NumberFormatException e){
				return "Not numeric";
			}
			return newdoubleformat(format, number);
		}
		return newinput;
	}

	//Syntax error for date format
    public String getOtherDateFormat(String format, String date) {
 		Date oldDate;
		String newformat;
		String newinput = "";
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat(format, Locale.ENGLISH);
	
		//parse date according to format
		try	{
			oldDate = formatter.parse(date);	
		}
		catch (ParseException e) {
			System.out.println("Date parsing failed");		
			return newinput;	
		}
		
		//select one random method of the following seven options								
		switch (RANDOM.nextInt(1, 7)){	
			case 1:
				newformat = switchMonthandDay(format);
				break;
				
			case 2:
				newformat = switchDateSeperator(format);	
				break;

			case 3:
				newformat = switchYearPosition(format);	
				break;					

			case 4:
				String a = switchMonthandDay(format);	
				newformat = switchDateSeperator(a);		
				break;

			case 5:
				String b = switchDateSeperator(format);	
				newformat = switchYearPosition(b);	
				break;	

			case 6:
				String c = switchMonthandDay(format);				
				String d = switchDateSeperator(c);	
				newformat = switchYearPosition(d);	
				break;	
				
			default:
                newformat = "yyyyy.MMMMM.dd"; 
				break;
		 }		
		formatter.applyPattern(newformat);						
		newinput = formatter.format(oldDate);

		return newinput;
	}


	//switch integer format to double 
	private String newintformat(String format, String number) {		
		String newinput = number;
		if (RANDOM.nextInt(1, 3) == 1) {
			double numberDouble = Double.parseDouble(number);
			newinput = String.format("%.3f", numberDouble);				
		}
		else {
			int numberInt = Integer.parseInt(number);
			newinput = String.format("%04d", numberInt);
		}
		
		return newinput;
	}

	//transform double format to exponential
	private String newdoubleformat(String format, String number) {		
		String newinput = number;
		NumberFormat numFormat = new DecimalFormat();
		if (RANDOM.nextInt(1, 2) == 1) {
			double numberDouble = Double.parseDouble(number);
			newinput = String.format("%.3f", numberDouble);				
			numFormat = new DecimalFormat("0.###E0");	    
			newinput = numFormat.format(numberDouble);
		}
		
		return newinput;
	}


	//switch Month and Day in date format
     private String switchMonthandDay(String oldformat) {												
		String str1 = oldformat;
		while (str1.contains("MMM")) {
			str1 = str1.replace("MMM", "MM");
		}			
		String str2 = str1.replace("d", "8");
		String str3 = str2.replace("M", "d");
		String newformat = str3.replace("8", "M");					

        return newformat;
    }

	//switch year from end to front or reverse
     private String switchYearPosition(String oldformat) {												
		String newformat = "yyyy%MMM";
		int i;
		String y ="";
		String ch = "";
		if (oldformat.contains("yyyy")) {
			y = "yyyy";
		}
		else if (oldformat.contains("yy")) {
			y = "yy";
		}
			
		if (oldformat.indexOf("MM") < oldformat.indexOf("dd")) {
			i = oldformat.indexOf("dd") + 2;
		}
		else if (oldformat.indexOf("dd") < oldformat.indexOf("MM")) {
			i = oldformat.indexOf("MM") + 2;
		}
		else {
			return newformat;
		}
		
		if (oldformat.indexOf(y) < oldformat.indexOf("MM")) {
			String str1 = oldformat.replace(y, "");
			char ch1 = str1.charAt(0);
			if (ch1 != 'd' && ch1 != 'M') {
				ch = Character.toString(ch1);
			}
			i = i - y.length();
			newformat = str1.substring(1, i).concat(ch).concat(y).concat(str1.substring(i, str1.length()));
		}
		else if (oldformat.indexOf(y) > oldformat.indexOf("MM")) {
			char ch1 = oldformat.charAt(oldformat.indexOf('y')-1);
			if (ch1 != 'd' && ch1 != 'M') {
				ch = Character.toString(ch1);
			}
			String str1 = oldformat.replace((ch.concat(y)), "");
			newformat = y.concat(ch).concat(str1);	
		}
		else {
			return newformat;
		}

        return newformat;
    }
	 

	//switch the seperator in the date	 
	 private String switchDateSeperator(String oldformat) {												
		String newformat;
		newformat = oldformat.replace("-", "&");
		newformat = newformat.replace("/", "-");		
		newformat = newformat.replace(".", "/");	
		newformat = newformat.replace("&", ".");	

        return newformat;
    }
 
}
