package de.hda.fbi.datagenerator.generators;

import java.util.HashSet;
import java.util.Iterator;

public class CustomDerivedValueListCollector {
	private static HashSet<CustomDerivedValueList> listOfCustomLists = new HashSet<CustomDerivedValueList>();

    public static CustomDerivedValueList findOrCreateCustomDerivedValueList(String name) {

        //iterate over all existing customDerivedValueList and check if there is already an existing one
        Iterator<CustomDerivedValueList> customListsIterator = listOfCustomLists.iterator();
        while (customListsIterator.hasNext()) {
			
            CustomDerivedValueList currentlyInspectedcustomList = customListsIterator.next();
            if (currentlyInspectedcustomList.getName().equals(name)) {
				        return currentlyInspectedcustomList;
			}
        }

        //no match found, create a new one
        CustomDerivedValueList newCustomDerivedList = new CustomDerivedValueList();
        newCustomDerivedList.setName(name);
        listOfCustomLists.add(newCustomDerivedList);
		newCustomDerivedList.readFile(name);
        return newCustomDerivedList;
    }
}
