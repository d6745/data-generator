package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.io.File;

@Function(name = "customSet")
public class CustomSet extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {
	protected static final String FILE_SEP = System.getProperty("file.separator");
    private static final String DICTIONARY_FOLDER = DICTIONARY + FILE_SEPERATOR;

	//function to read item from a given filename in the dictionary folder
    @FunctionInvocation
    public String getSet(String filename) {
		
		//check if string is null or empty
		if (filename == null || filename.isEmpty()) {
			return "";
		}
		
		String DICTIONARY_FILE =  DICTIONARY_FOLDER + filename; 
		
		//check if file exists
		File f = new File(System.getProperty("user.dir") + FILE_SEP + "src" + FILE_SEP + "main" + FILE_SEP + "resources" + FILE_SEP + DICTIONARY_FILE);	
		if(f.exists() && !f.isDirectory()) { 
				//read item from file
				String firstName = super.readItem(DICTIONARY_FILE);
				firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
				return firstName;
		}
		else {
			return filename;
		}
    }
	
}
