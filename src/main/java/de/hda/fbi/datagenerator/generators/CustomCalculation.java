package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.util.Random;
import java.lang.Math;
import java.math.*;

@Function(name = "calculate")
public class CustomCalculation implements ICustomGenerator {
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
private static final Random random = new Random();;	
double parsedv1; 
double parsedv2;
boolean db;

	//constructor with optional seed
	public CustomCalculation () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
		}
	}


   @FunctionInvocation
    public String getCustomCalculation(String calculation, String value_1, String value_2) {
		//check if input is double
		if (value_1 == "NaN") {value_1 = "not numeric";}
		if (value_2 == "NaN") {value_2 = "not numeric";}
		
		try{
			parsedv1 = Double.parseDouble(value_1);
			parsedv2 = Double.parseDouble(value_2);
			db = true;
		} catch(NumberFormatException e){
			db = false;
		}

		//if input is double perform and return calculation											
		if (!db) {return "NaN";}
		else {
			return calculate(calculation, parsedv1, parsedv2);
		}
	}


	//function with error
    public String getCustomCalculationwithErrors(String calculation, String value_1, String value_2) {
		if (value_1 == "NaN") {value_1 = "not numeric";}
		if (value_2 == "NaN") {value_2 = "not numeric";}	
		//check if input is double
		try{
			parsedv1 = Double.parseDouble(value_1);
			parsedv2 = Double.parseDouble(value_2);
			db = true;
		}
		catch(NumberFormatException e){
			db = false;
		}
			
		if (db == false) {return "NaN";}
			else {

		//pull random different operator for execution and return result
		String newcalculation = calculation;
		while (calculation.equals(newcalculation)) {
			String[] list = {"+", "-", "*", "/", "^"};
			newcalculation = list[random.nextInt(list.length)];
		}
		String newinput = calculate(newcalculation, parsedv1, parsedv2);	
		return newinput;
		}
	}


	//function to perform calculation with given operator and two operands
	private static String calculate(String calc, double value1, double value2) {
	
		switch (calc){
			case "+":					
					return Double.toString(round((value1 + value2), 2));	
					
			case "-":
					return Double.toString(round((value1 - value2), 2));	
			
			case "*":
					return Double.toString(round((value1 * value2), 2));
					
			case "/":
					if (value2 == 0) {
						return "NaN";
					}
					else {
						return Double.toString(round((value1 / value2), 2));
					}
					
			case "^":
					return Double.toString(Math.pow(value1, value2));
					
			default:
                return " no valid calculation operator ";
            
		}	
	
	
	}

	//rounding function
	private static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();
	
		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
