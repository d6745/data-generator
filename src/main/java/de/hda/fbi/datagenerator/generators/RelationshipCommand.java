package de.hda.fbi.datagenerator.generators;

/**
 * Created by marklukasmoller on 22.12.20.
 */
public enum RelationshipCommand {
    INIT, INITTOVALUE, INCREMENT, REPEAT, RANDOM
}
