package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customProduct")
public class CustomProduct extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "unibench-productTitles.txt";

    @FunctionInvocation
    public String getCustomProduct() {
        return super.readItem(DICTIONARY_FILE);
    }
}
