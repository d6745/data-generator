package de.hda.fbi.datagenerator.generators;

import java.util.*;
import java.lang.*;
import java.util.SplittableRandom;

public class ErrorIntervall {
	
private static SplittableRandom RANDOM = new SplittableRandom();	
private static final Random random = new Random();
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
	
	//constructor with optional seed
	public ErrorIntervall () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}
	

    public String getIntervallError(String min, String max) {			

		//check if input is integer
		boolean integers = true;
		int parsedIntmin = 0;
		int parsedIntmax = 0;
		try{
			parsedIntmin = Integer.parseInt(min);
			parsedIntmax = Integer.parseInt(max);
		}catch(NumberFormatException e){
			integers = false;		
		}
		
		//check if double
		boolean doubles = true;
		double parsedDoublemin = 0.0;
		double parsedDoublemax = 0.0;
		try{
			parsedDoublemin = Double.parseDouble(min);
			parsedDoublemax = Double.parseDouble(max);	
		}catch(NumberFormatException e){
			doubles = false;
		}

		//get type depended intervall error
		if (integers){
			String newinput = getIntIntervallError(parsedIntmin, parsedIntmax);
			return newinput;
		}		
		else if (doubles){
			String newinput = getDoubleIntervallError(parsedDoublemin, parsedDoublemax);
			return newinput;
		}
		else {
			return "";	
		}
	}

	//function to get double intervall error (+/- 10x span)
    private String getDoubleIntervallError(double min, double max) {
			double span = max - min;
			if (random.nextBoolean()) {
					return Double.toString(RANDOM.nextDouble((min-10.0*span), min));
				}
				else {
					return Double.toString(RANDOM.nextDouble(max, (max+10.0*span)));
				}
		}
		
	//function to get integer intervall error (+/- 10x span)
	private String getIntIntervallError(int min, int max) {
			int span = max - min;
						
			if (random.nextBoolean()) {
				return Integer.toString(RANDOM.nextInt((min-10*span), min));

			}
			else {
				return Integer.toString(RANDOM.nextInt(max, (max+10*span)));
			}
	}
	
}
