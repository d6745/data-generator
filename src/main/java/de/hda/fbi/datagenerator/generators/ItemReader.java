package de.hda.fbi.datagenerator.generators;

public interface ItemReader {

    String readItem(String filename);
}
