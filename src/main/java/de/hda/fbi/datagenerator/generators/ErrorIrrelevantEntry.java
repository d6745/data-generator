package de.hda.fbi.datagenerator.generators;

import java.util.*;
import java.lang.StringBuilder;
import java.util.Random;
import java.util.SplittableRandom;

public class ErrorIrrelevantEntry {
private static SplittableRandom RANDOM = new SplittableRandom();	
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();

	//constructor with optional seed
	public ErrorIrrelevantEntry () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

    public String getIrrelevantEntry(String category) {
		
		 //random decision which error to introduce (equal chances), type dependend
		 switch (category){

			case "word":
				String[] alist = {"Test", "Test_A", "Test_123", "Test_1234", "Test_432", "Test_MaxMustermann", "TestRun", "Sample_Entry1", "EngineerRun"};
				return alist[RANDOM.nextInt(alist.length)];
				
			case "firstName":
				String[] blist = {"Max", "John"};
				return blist[RANDOM.nextInt(blist.length)];
				
			case "lastName":
				String[] clist = {"Mustermann", "Doe"};
				return clist[RANDOM.nextInt(clist.length)];
			
			case "number":
				String[] dlist = {"1234", "12345", "0000", "54321", "0123456789"};
				return dlist[RANDOM.nextInt(dlist.length)];
				
			default:
                return "Test"; 
		 }
	}
		
}
