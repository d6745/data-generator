package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customPost")
public class CustomPost extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "unibench-postContent.txt";

    @FunctionInvocation
    public String getCustomPost() {
        return super.readItem(DICTIONARY_FILE);
    }
}
