package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customFirstName")
public class CustomFirstName extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "firstnames.txt";

    @FunctionInvocation
    public String getCustomFirstName() {

        String firstName = super.readItem(DICTIONARY_FILE);
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
        return firstName;
    }
}
