package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customIndustry")
public class CustomIndustry extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "industries.txt";

    @FunctionInvocation
    public String getCustomIndustry() {

        String firstName = super.readItem(DICTIONARY_FILE);
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
        return firstName;
    }
}
