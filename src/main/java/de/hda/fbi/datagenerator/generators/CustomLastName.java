package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customLastName")
public class CustomLastName extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "lastnames.txt";

    @FunctionInvocation
    public String getCustomLastName() {

        String lastName = super.readItem(DICTIONARY_FILE);
        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1);
        return lastName;
    }
}
