package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.util.*;
import java.lang.*;


@Function(name = "unique")
public class Unique implements ICustomGenerator {

private String attribute;
private static SplittableRandom RANDOM = new SplittableRandom();
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();

	//constructor with optional seed
	public Unique () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

@FunctionInvocation
    public String getUnique(String attribute, String input) {			

	//get instance of unique list	
	UniqueList concreteUnique = UniqueList.getInstance();

	//check whether attribute entry was already used
	String newinput = concreteUnique.checkUnique(attribute, input);

	//if attribute entry was already used, mark as duplicate
		if (newinput == "!Duplikat!") {
			return "- DUPLICATE -";
		}
		else {
			return newinput;
		}
	}

}