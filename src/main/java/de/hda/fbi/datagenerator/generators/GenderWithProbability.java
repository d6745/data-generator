package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.util.EmptyStackException;
import java.util.SplittableRandom;


@Function(name = "genderWithProbability")
public class GenderWithProbability implements ICustomGenerator {

    private static final SplittableRandom RANDOM = new SplittableRandom();

    @FunctionInvocation
    public String getGenderWithProbalitity(String pprobability) throws EmptyStackException {

        try {
            int probability = Integer.parseInt(pprobability);

            if (RANDOM.nextInt(1, 101) <= probability){
                if (RANDOM.nextInt(1, 101) <= 50) {
                    return "\"gender\":\"male\",";
                }
                return "\"gender\":\"female\",";
            }

            /*
            if (new Random().nextInt(probability) == 0) {
                if (new Random().nextInt() % 2== 0) {
                    // Notice: Start and end brackets are created by the generator parser
                    return "gender\": \"male";
                }
                return "gender\": \"female";
            }
            */

        } catch (NumberFormatException e) {
            return "";
            //return "NOPROPERTY";
        }

        return "";
        //return "NOPROPERTY";
    }
}
