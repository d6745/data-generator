package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;


@Function(name = "customFeedback")
public class CustomFeedback extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final String DICTIONARY_FILE = DICTIONARY + FILE_SEPERATOR + "unibench-feedbacks.txt";

    @FunctionInvocation
    public String getCustomFeedback() {
        return super.readItem(DICTIONARY_FILE);
    }
}
