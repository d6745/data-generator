package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;
import java.io.*;
import java.util.*;
import java.lang.*;

@Function(name = "customDerivedValue")
public class CustomDerivedValueList extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {
protected static final String FILE_SEP = System.getProperty("file.separator");
private	ErrorCollector concreteErrors = ErrorCollector.getInstance();
private static CustomDerivedValueList instance;
private String listname;
private static SplittableRandom RANDOM = new SplittableRandom();
private static Map<String, String> dList = new HashMap<String, String>();
private static final String DICTIONARY_FOLDER = DICTIONARY + FILE_SEPERATOR;


	public CustomDerivedValueList () {
		if (concreteErrors.s > 0) {
			RANDOM = new SplittableRandom(concreteErrors.s);
		}
	}

    @FunctionInvocation
    public String getCustomDerivedValueList(String filename, String key){

        //check all CustomDerivedValueList if a list with the name was yet defined
        //if so, reuse it; If not, create it;
        CustomDerivedValueList concreteCustomDerivedValueList = CustomDerivedValueListCollector.findOrCreateCustomDerivedValueList(filename);

		return concreteCustomDerivedValueList.getValue(key);
	}
	

   @FunctionInvocation
    public String getCustomDerivedValueListwithErrors(String filename, String key, String input) {

        CustomDerivedValueList concreteCustomDerivedValueList = CustomDerivedValueListCollector.findOrCreateCustomDerivedValueList(filename);
		
		int i = 0;	
		String newinput = input;
			
		//draw random values from map as long as the value did not change (or we tried 10 times)
		while (newinput.equals(input) && i < 10) {			
			Object[] values = dList.values().toArray();
			newinput = (String) values[RANDOM.nextInt(values.length)];
			i += 1;
		}
		//in case no other values was found, newinput is set to empty;
		if (newinput.equals(input)) {newinput = "";}
		
		return newinput;

	}	

	//get corresponding value for key
    public String getValue(String entrykey) {
		if (!this.dList.containsKey(entrykey)) {
			return "no_valid_input";
        }
		else {		
			return	dList.get(entrykey);
		}
    }

    public String getName() {
        return this.listname;
    }

    public void setName(String name) {
        this.listname = name;
    }	

	
    public void readFile(String filename) {
		try {
			for(Scanner sc = new Scanner(getClass().getClassLoader().getResourceAsStream(DICTIONARY_FOLDER + filename)); sc.hasNext(); )
			{
				String[] parts = sc.nextLine().split(":", 2);
				if (parts.length >= 2) {
					String key = parts[0];
					String value = parts[1];
					dList.put(key, value);
				} 
			}
		} catch (Exception ex) {
            System.out.println("ERROR - reading file!");
            System.out.println(ex);
        }
	
        /*
		String line;
		String filePath = System.getProperty("user.dir") + FILE_SEP + "src" + FILE_SEP + "main" + FILE_SEP + "resources" + FILE_SEP + DICTIONARY_FOLDER + filename;
		//check if file exists (provide error message if not)
		File f = new File(filePath);
		if(f.exists() && !f.isDirectory()) { 
			//read file (provide error message if it does not work)
			try {
				BufferedReader reader = new BufferedReader(new FileReader(filePath));
				while ((line = reader.readLine()) != null) {
					//file with ":" to separate key and value is expected
					String[] parts = line.split(":", 2);
					if (parts.length >= 2) {
						String key = parts[0];
						String value = parts[1];
						dList.put(key, value);
					} 
				}
				reader.close();
			} 
			catch (IOException e) {
				System.out.println("An error occurred while reading the file: " + filePath);
				e.printStackTrace();
			}
		}
		else {
			System.out.println("File does not exist: " + filePath);
		}
		*/

    }	


}