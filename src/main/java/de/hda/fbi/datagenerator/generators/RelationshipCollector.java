package de.hda.fbi.datagenerator.generators;

import java.util.HashSet;
import java.util.Iterator;

public class RelationshipCollector {
    static HashSet<RelationshipGenerator> listOfRelationships = new HashSet<RelationshipGenerator>();

    public static RelationshipGenerator findOrCreateRelationship(String name) {

        // Iterate over all existing relationships and check if there is already an existing one

        Iterator<RelationshipGenerator> relationshipsIterator = listOfRelationships.iterator();

        while (relationshipsIterator.hasNext()) {
            RelationshipGenerator currentlyInspectedRelationship = relationshipsIterator.next();
            //System.out.println(currentlyInspectedRelationship.getRelationshipName());
            if (currentlyInspectedRelationship.getRelationshipName().equals(name)) {
                //System.out.println("Existing relationship found");
                return currentlyInspectedRelationship;
            }
        }

        // no matching relationship found, create a new one

        RelationshipGenerator newRelationship = new RelationshipGenerator();
        newRelationship.setRelationshipName(name);
        listOfRelationships.add(newRelationship);
        //System.out.println("Creating a new relationship");
        return newRelationship;
    }
}
