package de.hda.fbi.datagenerator.generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.util.Random;
import java.util.stream.Collectors;


@Function(name = "customRandomArray")
public class CustomRandomIntArray  implements ICustomGenerator {
	public static ErrorCollector concreteErrors = ErrorCollector.getInstance();
    Random random = new Random();
	
	public CustomRandomIntArray () {
		if (concreteErrors.s > 0) {
			random.setSeed(concreteErrors.s);
		}
	}

    @FunctionInvocation
    public String getRandomArray(String min_arr_length, String max_array_length, String contentRangeMin, String contentRangeMax, String exclude)
    {

        int parsed_min_arr_length=0, parsed_max_array_length=0, parsed_contentRangeMin=0, parsed_contentRangeMax=0, parsed_exclude=0;
        try {
            parsed_min_arr_length = Integer.parseInt(min_arr_length);
            parsed_max_array_length = Integer.parseInt(max_array_length);
            parsed_contentRangeMin = Integer.parseInt(contentRangeMin);
            parsed_contentRangeMax = Integer.parseInt(contentRangeMax);
            parsed_exclude = Integer.parseInt(exclude);

        } catch (NumberFormatException ex) {
            System.out.println("ERROR - parsing String to int!");
            System.exit(1);
        }

        if (parsed_exclude > parsed_contentRangeMax || parsed_exclude < parsed_contentRangeMin) {
            System.out.println("WARNING - exclude out of range!");
        }

        int content_size = (parsed_contentRangeMax - 1);

        if (parsed_max_array_length > (content_size)) {
            System.out.println("WARNING - max out of range!, " + max_array_length + "; " + content_size);
            parsed_max_array_length = content_size;
        }

        int arr_size = random.nextInt((parsed_max_array_length - parsed_min_arr_length) + 1) + parsed_min_arr_length;

        int finalParsed_exclude = parsed_exclude;
        return random.ints(parsed_contentRangeMin, parsed_contentRangeMax+1).filter(i -> i != finalParsed_exclude)
                .distinct().limit(arr_size).boxed()
                //.map(s -> "\""+s+"\"") // String content!
                .collect(Collectors.toList()).toString().replaceAll(" ", "");
    }
}
