package de.hda.fbi.datagenerator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.Map;
import java.util.Set;

// https://github.com/simplymequeeny/json-string-schema-generator

public class SchemaGenerator
{
    public SchemaGenerator() {}

    public JsonObject getSchemaFromJsonObject(JsonObject jsonObject, String title)
    {
        //System.out.println("Creating Schema: " + title);

        JsonObject schema = new JsonObject();

        schema.addProperty("$schema", "http://json-schema.org/draft-04/schema#");
        schema.addProperty("title", title);
        schema.addProperty("type", "object");


        // add the properties to the schema
        schema.add("properties", getJsonObjectProperties(jsonObject));

        schema.addProperty("additionalProperties", true);

        return schema;
    }



    private JsonObject getJsonObjectProperties(JsonObject jsonObject)
    {
        //System.out.println("Getting properties");

        JsonObject properties = new JsonObject();

        Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
        for(Map.Entry<String,JsonElement> entry : entrySet){

            String key = entry.getKey();
            JsonElement jsonElement = entry.getValue();

            //System.out.println("Handle property key: " + key);

            // PRIMITIVE TYPE
            if (jsonElement.isJsonPrimitive()) {

                //System.out.println("Value type: JSON_PRIMITIVE");

                JsonPrimitive p = entry.getValue().getAsJsonPrimitive();
                // STRING
                if (p.isString()) {
                    //System.out.println("  IS STRING");
                    properties.add(key, getTypeObject("string"));
                }
                // NUMBER
                else if (p.isNumber()) {
                    //System.out.println("  IS NUMBER");
                    if (p.getAsString().contains(".")) {
                        properties.add(key, getTypeObject("number"));
                    } else {
                        properties.add(key, getTypeObject("integer"));
                    }
                }
            }
            // ARRAY
            else if (jsonElement.isJsonArray()) {
                //System.out.println("Value type: JSON_ARRAY");

                // the array schema object
                JsonObject array_object = new JsonObject();
                array_object.addProperty("type", "array");

                // the items (types) of from the array
                JsonArray items = new JsonArray();

                // get the first element from the json data array
                // TODO: Loop over each array element and check for different types!!!
                JsonElement array_element = jsonElement.getAsJsonArray().get(0);

                // PRIMITIVE TYPE
                if (array_element.isJsonPrimitive()) {

                    //System.out.println("First json array type: JSON_PRIMITIVE");

                    JsonPrimitive p = array_element.getAsJsonPrimitive();
                    // STRING
                    if (p.isString()) {
                        //System.out.println("  IS STRING");
                        items.add(getTypeObject("string"));
                    }
                    // NUMBER
                    else if (p.isNumber()) {
                        //System.out.println("  IS NUMBER");
                        if (p.getAsString().contains(".")) {
                            items.add(getTypeObject("number"));
                            // Darwin cannot parse number type!!!
                            //items.add(getTypeObject("integer"));
                        } else {
                            items.add(getTypeObject("integer"));
                        }
                    }
                }
                // JSON OBJECT
                else if(array_element.isJsonObject()) {

                    //System.out.println("First json array type: JSON_OBJECT");

                    JsonObject type_object = new JsonObject();
                    type_object.addProperty("type","object");

                    //System.out.println("RECURSIVE CALL.....");
                    type_object.add("properties", getJsonObjectProperties((JsonObject) array_element));
                    type_object.addProperty("additionalProperties", true);

                    items.add(type_object);

                }
                else {
                    System.out.println("ERROR - arrays can only contain primitive types!");
                }

                // add items array
                array_object.add("items", items);

                // add "additionalItems"
                array_object.addProperty("additionalItems", false);


                // add to array to schema properties
                properties.add(key, array_object);

            }
            // OBJECT
            else if (jsonElement.isJsonObject()) {
                //System.out.println("ERROR - cannot handle object type!");
                JsonObject type_object = new JsonObject();
                type_object.addProperty("type","object");

                //System.out.println("RECURSIVE CALL.....");
                type_object.add("properties", getJsonObjectProperties((JsonObject) jsonElement));

                properties.add(key, type_object);

            }
            // NULL
            else if (jsonElement.isJsonNull()) {
                //System.out.println("ERROR - cannot handle null type");
                properties.add(key, getTypeObject("null"));
            }
        }

        return properties;
    }



    private JsonObject getTypeObject(String type) {
        JsonObject type_object = new JsonObject();
        type_object.addProperty("type",type);
        return type_object;
    }

}
