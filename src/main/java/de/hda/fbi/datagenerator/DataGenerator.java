package de.hda.fbi.datagenerator;

import com.github.vincentrussell.json.datagenerator.JsonDataGenerator;
import com.github.vincentrussell.json.datagenerator.JsonDataGeneratorException;
import com.github.vincentrussell.json.datagenerator.functions.FunctionRegistry;
import com.github.vincentrussell.json.datagenerator.impl.JsonDataGeneratorImpl;
import de.hda.fbi.datagenerator.generators.ICustomGenerator;
import de.hda.fbi.datagenerator.generators.ErrorCollector;
import de.hda.fbi.datagenerator.generators.ErrorToGroundTruth;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.reflections.Reflections;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.io.*;
import java.util.Arrays;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.time.LocalTime;

public class DataGenerator {

    public static final String FILE_SEPERATOR = System.getProperty("file.separator");
	public static ErrorCollector concreteErrors = ErrorCollector.getInstance();
	
    public static void main(String[] args) {

        Options options = new Options();

        Option input = new Option("s", "schemaPath", true, "path to schema folder");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "outputPath", true, "output directory");
        output.setRequired(true);
        options.addOption(output);

        Option seed = new Option("f", "value", true, "given seed");
        seed.setRequired(false);
        options.addOption(seed);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("datagenerator", options);
            System.exit(1);
        }

        String SCHEMA_DIR_PATH = cmd.getOptionValue("schemaPath");
        String TARGET_DIR_PATH = cmd.getOptionValue("outputPath");
		String fixvalue = cmd.getOptionValue("value");

        try {
            int parsedfixvalue = Integer.parseInt(fixvalue);
			concreteErrors.s = parsedfixvalue;
			System.out.println("Seed set to: " + parsedfixvalue);
        } catch (NumberFormatException e) {
            System.out.println("The given seed value is not compatible");
        }



        System.out.println("## Data Generator for Darwin/EvoBench\n");

        System.out.println("Schema path: " + SCHEMA_DIR_PATH);
        System.out.println("Output path: " + TARGET_DIR_PATH + "\n");


        // -- Auto-register all generators implementing the ICustomGenerator interface
        try {
            Reflections reflections = new Reflections("de.hda.fbi.datagenerator.generators");
            Set<Class<? extends ICustomGenerator>> classes = reflections.getSubTypesOf(ICustomGenerator.class);

            System.out.println("Registered custom generators:");
            for (Class generator : classes) {
                FunctionRegistry functionRegistry = FunctionRegistry.getInstance();
                functionRegistry.registerClass(generator);
                System.out.println("  " + generator.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("");

        // -- Scan schema folder for schema files to process
        File schemaDirectory = new File(SCHEMA_DIR_PATH);

        FileFilter fileFilter = new WildcardFileFilter("*.schema", IOCase.INSENSITIVE);
        File[] listOfSchemas = schemaDirectory.listFiles(fileFilter);
        Arrays.sort(listOfSchemas);

        // Create target directory if not exist
        File targetDirectory = new File(TARGET_DIR_PATH);
        if (! targetDirectory.exists()){
            System.out.println("Output path, " + TARGET_DIR_PATH + " does not exist! creating it...");
            targetDirectory.mkdir();
        }

        System.out.println("");

		// get Time before data generation starts
		LocalTime timebefore = LocalTime.now();

        try {
            JsonDataGeneratorImpl schema_parser = new JsonDataGeneratorImpl();

            for (File schema : listOfSchemas) {
                System.out.println("Generating data from Schema: " + schema.toString() + "...");
				//inform ErrorCollection about the currently processed schema
				concreteErrors.currentschema = FilenameUtils.removeExtension(schema.getName()).substring(3);
                // Get schema filename, e.g. Customer.schema.bak, remove extension and add .json as extension for the output
                String outputFileName = FilenameUtils.removeExtension(schema.getName()).substring(3) + ".json";

                //File outputFile = new File(TARGET_DIR_PATH + FILE_SEPERATOR + outputFileName);
                //EntityTypeGenerator generator = new EntityTypeGenerator(schema, outputFile);
                //generator.generateEntitiesToFile();

                InputStream in_file_stream = new FileInputStream(schema);
                FileOutputStream out_file_stream = new FileOutputStream(TARGET_DIR_PATH + FILE_SEPERATOR + outputFileName);
                BufferedOutputStream out_stream_buffer = new BufferedOutputStream(out_file_stream);
  
				// create the data
                schema_parser.generateTestDataJson(in_file_stream, out_stream_buffer);
                out_stream_buffer.close();
            }
        } catch (Exception e) {
            System.out.println("Error - processing data");
            System.out.println(e);
            //System.exit(0);
        }

		// get Time after data generation
		LocalTime timeafter = LocalTime.now();


		//Write included errors to file
		concreteErrors.sort();
		concreteErrors.writeErrorFile(TARGET_DIR_PATH + FILE_SEPERATOR);

		// get Time after writing error file
		LocalTime timeErrorFile = LocalTime.now();

        System.out.println("\n## Validating generated data");
        JsonValidator jsonValidator = new JsonValidator();
        //File[] jsonFiles = jsonValidator.validateFolder( TARGET_DIR_PATH  );
        jsonValidator.validateFolder( TARGET_DIR_PATH  );

		// get Time after file validation
		LocalTime timeValidation = LocalTime.now();

		//Generate Ground Truth
		ErrorToGroundTruth errortgt = new ErrorToGroundTruth();
		errortgt.writeGroundTruth(TARGET_DIR_PATH);

		// get Time after writing error file
		LocalTime timeGroundTruth = LocalTime.now();

		System.out.println(" ");
		System.out.println("Data Generation Start " + timebefore);
		System.out.println("Data generated " + timeafter);
		System.out.println("ErrorFile written " + timeErrorFile);
		System.out.println("DataSets validated " + timeValidation);
		System.out.println("GroundTruth written " + timeGroundTruth);

        // Read files from folder
        File datadir = new File( TARGET_DIR_PATH );
        File[] jsonFiles = datadir.listFiles((dir, name) -> {
            if (name.toLowerCase().endsWith(".json")) {
                return true;
            } else {
                return false;
            }
        });

        if (jsonFiles != null) {
            try {
                System.out.println("\n## Generating zip archive ...");
                String[] parts = TARGET_DIR_PATH.split(FILE_SEPERATOR);
                String zip_file_path = TARGET_DIR_PATH + FILE_SEPERATOR + parts[parts.length-1] + ".zip";
                ByteArrayOutputStream fos = new ByteArrayOutputStream();
                ZipOutputStream zipOut = new ZipOutputStream(fos);
                for (File fileToZip : jsonFiles) {
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                    zipOut.putNextEntry(zipEntry);
                    IOUtils.copy(fis, zipOut);
                }
                zipOut.close();
                fos.close();
                OutputStream outputStream = new FileOutputStream(zip_file_path);
                fos.writeTo(outputStream);

            } catch (IOException ex) {
                System.out.println("ERROR - generating zip archive");
                System.out.println(ex);
            }
        }
		
    }
}
