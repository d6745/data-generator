# GouDa - Generation of universal Data sets
In the following, a description for generating test data sets with GouDa is given.
The required Docker containers are available on the [Container Registry](https://gitlab.com/d6745/data-generator/container_registry).

**Contents**
- [GouDa - Generation of universal Data sets](#gouda---generation-of-universal-data-sets)
	- [Quickstart](#quickstart)
	- [Usage](#usage)
		- [Python](#python)
		- [Java](#java)
	- [Available error generating functions](#available-error-generating-functions)
		- [Level 1: An Attribute Value of a Single Tuple](#level-1-an-attribute-value-of-a-single-tuple)
		- [Level 2: The Values of a Single Attribute](#level-2-the-values-of-a-single-attribute)
		- [Level 3: The Attribute Values of a Single Tuple](#level-3-the-attribute-values-of-a-single-tuple)
		- [Level 4: The Attribute Values of Several Tuple](#level-4-the-attribute-values-of-several-tuple)
		- [Added data generating functions](#added-data-generating-functions)
		- [Added data generating functions](#added-data-generating-functions-1)
	- [Add new error type](#add-new-error-type)
	- [Add new generator](#add-new-generator)


**Note:** In the [Package Registry](https://gitlab.com/d6745/data-generator/-/packages/5585425) you can find examples of data sets generated with GouDa. 


## Quickstart
For a quickstart, the only requirement is to have [Docker](https://www.docker.com/) installed. With the following command a short test run can be executed:
```bash
docker run -t --rm \
-v "$(pwd)/GouDa-datasets:/opt/datagenerator/datasets" \
registry.gitlab.com/d6745/data-generator:20220321.1
```

This uses the default entry point, which executes the script `generateErrorData_TestSet.py` with a scaling factor of 1. Alternatively, the use of other python scripts and other scaling factors is possible. See [section "Python"](#python) for more information about the available scripts and scaling factors. An example command would be as follows:
```bash
docker run -t --rm \
-v "$(pwd)/GouDa-datasets:/opt/datagenerator/datasets" \
registry.gitlab.com/d6745/data-generator:20220321.1 \
./scripts/generateErrorData_TreatmentSet.py 0.1
```

After that, the created data sets can be found in the "GouDa-datasets" folder.

## Usage
There are two options for starting the data generator:
- Run Python script
- Execute JAR File

Technical requirements:
- Python (at least 3)
- Java SE Development Kit (at least 17.0.1)
- Apache Maven	(at least 3.8.4)

### Python
A schema is needed as input. In this, the functions to be used for data generation are specified. When using Python, this schema is created in the script itself, including the call of the generator. Also, the folders where the created schema and data sets will be stored are defined in this script. Per default this is "./SCHEMA/" for the schema files and "./datasets/" for the data set files.

Python scripts with one or more schemas are provided (folder "scripts"):
1) `generateErrorData_PerformanceTestSet.py`
Simple schema used for performance testing of individual functions
2) `generateErrorData_TreatmentSet.py`
Replica of the dataset described in Goldberg et al. (2011)
3) `generateErrorData_TestSet.py`
Extensive data schema in which all created functions are implemented.

For this, the source code must first be compiled via Maven (cmd): `mvn -U clean compile`

Then, it can be run via the command `python ./scripts/<script_name> <scaling_factor>`, e.g. `python ./scripts/generateErrorData_TestSet.py 0.1`. Alternatively, instead of the scaling factor, the number of lines to be generated can be passed.

At least Python 3.8 is required.

### Java
Alternatively, the JAR file can be executed directly with the schemas as input. The file path for the schema as well as for the created data sets is specified in the call of the executable file. 

JAR Package with all dependencies can be created via Maven: `mvn -U clean package`

To execute it, run:
`java -jar -Xmx8g ./target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar -s SCHEMA/<schema> -o <out_path>`
Alternatively, a seed can be specified as well:
`java -jar -Xmx8g ./target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar -s SCHEMA/<schema> -o <out_path> -f <integer>`

The following is an example of a valid schema:
```
{{relationship("Performance_Test-id idx", "INIT", "1")}} # Initialization for relationships
['{{repeat(10000)}}', { # 1000 data objects
  "Index":{{relationship("Performance_Test-id-idx","INCREMENT","1")}}, # incremental index
  "City":"{{ city() }}", # No error, function for city names
  "Date":"{{ error("SYNTAX",5,"date","dd.MM.yyyy",date("08-05-1945 00:00:00","03-10-1990 00:00:00","dd.MM.yyyy")) }}", # date range with 5% syntax error
  "Count":"{{ error("INTERVALLVIOLATION",5,0,9999,integer(0,9999)) }}" # integer range with 5% interval violation
}]
```

## Available error generating functions

### Level 1: An Attribute Value of a Single Tuple

Missing values (empty strings) with given rate for arbitrary function / input:
```bash
	{{error("MISSINGVALUE", int rate, <function()>)}}
```

Syntax errors (format / pattern deviations) with given rate for the specified type and format :
```bash
types -> ("text", "date", "numeric"), formats -> (date: Java SimpleDateFormat, numeric: Java String Format Specifiers)
	{{error("SYNTAX", int rate, <type>, <format>, <function()>)}}
```

Intervall violation, adds values outside of a given intervall with given rate for double or integer inputs:
```bash
	{{error("INTERVALLVIOLATION", int rate, min, max, <function()>)}}
```

Set violation, adds inputs from given "violation set" text file with given rate for arbitrary function / input:
```bash
	{{error("SETVIOLATION", int rate, String <text file>, <function()>)}}
```

Spelling errors (switched chars, letter addition and removal) with given rate for arbitrary function / input:
```bash
	{{error("SPELLING", int rate, <function()>)}}
```

Inadequate value to the attribute context (from defined other context) for arbitrary function / input:
```bash
Option 1 - entry from another avaible attribute context:
	{{put(<key>, <function()>)}}
	{{error("CONTEXT", int rate, <key>, <function()>)}}
```

```bash
Option 2 - (entry from independent context: function_2)
	{{error("CONTEXT", int rate, <function_2()>, <function()>)}}
```

Value items beyond the attribute context (value from defined other context) with given rate for arbitrary function / input:
```bash
Option 1 - get entry from an attribute below (function_2):
	{{error("BEYOND_CONTEXT", int rate, <function_1()>, <function_2()>)}}
	{{error("BEYOND_CONTEXT_NEXT", <function_2()>)}}
```

```bash
Option 2 - get entry from a previous attribute (function_1):
	{{error("BEYOND_CONTEXT_NEXT", int rate, <function_1()>)}}
	{{error("BEYOND_CONTEXT", <function_1()>, <function_2()>)}}	
```

Nonsense values (random strings from numbers and letters) with given rate for arbitrary function / input:
```bash
	{{error("NONSENSE", int rate, <function()>)}}
```

Wrong values (values from another function: function_2) with given rate for arbitrary function / input (function_1):
```bash
	{{error("ALTERNATE", int rate, <function_2()>, <function_1()>)}}
```


### Level 2: The Values of a Single Attribute

Duplicates within an attribute with given rate for unique function / input:
```bash
	{{error("DUPLICATE_ATTR", int rate, String attribute_name, <unique(function())>)}}
```

Outliers, adds outliers in a given statistical distribution (accepts "equal", "normal" and "poisson") with given rate:
```bash
	{{error("OUTLIER", int rate, double mean, double variance, String <distribution>)}}
```


### Level 3: The Attribute Values of a Single Tuple

Semi empty tupel, with given rate in first affected attribute, for arbitrary function / input:
```bash
	{{error("EMPTY_TUPEL", int rate, <(function()>)}}
	{{error("EMPTY_TUPEL", <(function()>)}}
	{{error("EMPTY_TUPEL", <(function()>)}}
	... and so on	
```

Derived calculated value error with given rate for two arbitrary numeric inputs (works also with fixed parameters):
```bash
calculation operators -> ("+", "-", "*", "/", "^")
	{{put(<key_1>, <numeric function()>)}}
	{{put(<key_2>, <numeric function()>)}}
    {{error("CALCULATION_ERROR", int rate, String <calculation operator>, get(<key_1>), get(<key_2>))}}",
```

Irrelevant entries in tupel, with given rate in first affected attribute, with specified type:
```bash
types -> ("word", "number", "firstname", "lastname")
	{{error("IRRELEVANT", int rate, String <type>, <(function()>)}}
	{{error("IRRELEVANT", String <type>, <(function()>)}}
	{{error("IRRELEVANT", String <type>, <(function()>)}}
	... and so on	
```

Dependend value error with given rate to a given input from a custom file (in ".src\main\resources\dictionaries", file data format: "key:value")
```bash
	{{error("DERIVED_VALUE_ERROR", int rate, String filename, String key)}}
```


### Level 4: The Attribute Values of Several Tuple

Entity duplicate (with minimal deviation from original), with given rate in first affected attribute, for arbitrary function / input:
```bash
	{{error("DUPLICATE_ENTITY", int rate, <(function()>)}}
	{{error("DUPLICATE_ENTITY", <(function()>)}}
	{{error("DUPLICATE_ENTITY", <(function()>)}}
	... and so on	
```

Bias and Class Imbalance:
```bash
	{{alternate(int rate, <function_2()>, <function_1()>)}}
	{{alternate(<function_2()>, <function_1()>)}}
	... and so on	 	
```

### Added data generating functions

Add values from a statistical distribution with given mean and variance (accepts "equal", "normal" and "poisson"):
```bash
	{{customDistribution(String type, String mean, String variance)}}
```

Add values from function_2 instead of funtion_1 with given rate:
```bash
	{{alternate(int rate, <function_2()>, <function_1()>)}}
	{{alternate(<function_2()>, <function_1()>)}} 	
```

Add values from a custom given file (in ".src\main\resources\dictionaries"):
```bash
	{{customSet(String <filename.txt>)}} 
```

Force unique entries in an attribute (returns "- INVALID DUPLICATE -") instead of duplicates:
```bash
	{{unique(String <attributename>, <function()>)}}
```

Get the dependend values to a given input from a custom file (in ".src\main\resources\dictionaries", file data format: "key:value") 
```bash
	{{customDerivedValue(String filename, String key)}}
```

Returns calculation result of two parameters and given operator (accepted operators: "`*`", "/", "-", "+", "^") 
```bash
	{{calculate(String <operator>, String value_1, String value_2)}}
```

### Added data generating functions

Add values from a statistical distribution with given mean and variance (accepts "equal", "normal" and "poisson"):
```bash
	{{customDistribution(String type, String mean, String variance)}}
```

Add values from function_2 instead of funtion_1 with given rate:
```bash
	{{alternate(int rate, <function_2()>, <function_1()>)}}
	{{alternate(<function_2()>, <function_1()>)}} 	
```

Add values from a custom given file (in ".src\main\resources\dictionaries"):
```bash
	{{customSet(String <filename.txt>)}} 
```

Force unique entries in an attribute (returns "- INVALID DUPLICATE -") instead of duplicates:
```bash
	{{unique(String <attributename>, <function()>)}}
```

Get the dependend values to a given input from a custom file (in ".src\main\resources\dictionaries", file data format: "key:value") 
```bash
	{{customDerivedValue(String filename, String key)}}
```

Returns calculation result of two parameters and given operator (accepted operators: "`*`", "/", "-", "+", "^") 
```bash
	{{calculate(String <operator>, String value_1, String value_2)}}
```

## Add new error type
- Create "Error Command" in ErrorCommand.java
- Create error in class ErrorGenerator:
	- Select the appropriate getError() method depending on the required number of parameters passed (currently 2-5 parameters are covered)
	- Create error in switch-case (checks error command)
	- If necessary, check the passed error rate with: `rateCheck(rate);`
	- Simple errors (i.e. empty string, random string) can be implemented directly, more complex errors can be realized by corresponding classes
	- Determine by random number whether error is built in or not:
		```java
			if (RANDOM.nextInt(1, 101) <= parsedrate) {
				// create error, capture it and return it
			}
			else {
				// return right value (should be input parameter!)
			}
		```
- Capture data errors, when an error is generated it must be captured (for error list and ground truth, via the command: `concreteErrors.addError(parsedCommand.toString(), input, newinput);`
Error type, correct value and incorrect value are passed (internally the line and the error counter are captured as well). If a dependent function is used for the error, which must be executed again for the correction, the value must be recorded in case of error introduction and in case of non-error introduction, via:
`concreteErrors.addError(command, input, newinput, stringList);`
`concreteErrors.addError(command, input, "zzz_keinFehler", stringList);`
In the StringList the parameters are to be entered (example: see Calculation_Error).

Simple example:
```java
…case MISSINGVALUE:
	rateCheck(rate); // check for valid rate (0 – 100)
	if (RANDOM.nextInt(1, 101) <= parsedrate){ // decision if error is introduced
		concreteErrors.addError(parsedCommand.toString(), input, ""); // capture error
		return ""; // return error
	}
	else {
		return input; // return right value (if no error is introduced)
	}
```

## Add new generator
- New generators can be created. They are initialized as a function and can then be addressed via the schema. To do this, the new generator must be created in the following package: `package de.hda.fbi.datagenerator.generators`
In addition, the following classes must be imported (they accomplish the use as a function for data generation):
```
import com.github.vincentrussell.json.datagenerator.functions.Function
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation
```
- The new class must implement the abstract class `ICustomGenerator`
- Before the class declaration the name of the function can be defined: `@Function(name = "<Funktionsname>")`
- The statement `@FunctionInvocation` is placed in front of the executing function to indicate which method should be used when calling the function via the function name. This also works with multiple (overloaded) methods
- For a simple example, see `CustomSet.java`