FROM openjdk:17-alpine 


RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python

WORKDIR /opt/datagenerator

VOLUME /opt/datagenerator/datasets
VOLUME /opt/datagenerator/schema

COPY target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar target/datagenerator-1.0-SNAPSHOT-jar-with-dependencies.jar
COPY scripts scripts

ENTRYPOINT ["python"] 
CMD ["scripts/generateErrorData_TestSet.py", "1"] 
